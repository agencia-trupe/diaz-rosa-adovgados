-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 09/08/2013 às 13h28min
-- Versão do Servidor: 5.5.32
-- Versão do PHP: 5.4.15-1~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `drosa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `atuacoes`
--

CREATE TABLE IF NOT EXISTS `atuacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `imagem` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slug` (`titulo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Extraindo dados da tabela `atuacoes`
--

INSERT INTO `atuacoes` (`id`, `titulo`, `texto`, `imagem`) VALUES
(14, 'Direito Civil', '<p>&raquo; Planejamento<span class="example1"> Estrat&eacute;gico</span> Sist&ecirc;mico.<br />&raquo; Planejamento de Mudan&ccedil;a Organizacional.</p>', 'atuacao_1-civil.jpg'),
(22, 'Direito Societário', '<p>Teste teste teste teste</p>', 'atuacao_2-societario.jpg'),
(23, 'Direito do Terceiro Setor', '<p>Direito do terceiro setor</p>', 'atuacao_3-terceirosetor.jpg'),
(24, 'Direito do Trabalho', '<p>Direito do Trabalho</p>', 'atuacao_4-trabalho.jpg'),
(25, 'Direito Tributário', '<p>Direito Tribut&aacute;rio</p>', 'atuacao_5-tributario.jpg'),
(26, 'Direito Imobiliário', '<p>Direito Imobili&aacute;rio.</p>', 'atuacao_6-imobiliario.jpg'),
(27, 'Direito de Família e Sucessões', '<p>Direito de Fam&iacute;lia e Sucess&otilde;es</p>', 'atuacao_7-familia.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `boxes`
--

CREATE TABLE IF NOT EXISTS `boxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `boxes`
--

INSERT INTO `boxes` (`id`, `titulo`, `texto`) VALUES
(1, 'Instituições de Ensino', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus mollis, sem sagittis scelerisque pharetra, ligula diam vestibulum neque, ac feugiat felis lectus eget velit. Duis viverra rutrum arcu, id faucibus nibh porttitor a. Suspendisse sollicitudin nisi eget nunc interdum adipiscing. Nam imperdiet interdum faucibus. Sed id lectus vitae diam semper lacinia. Quisque vitae mi ac mi iaculis sagittis ullamcorper at ipsum.</span></p>'),
(2, 'Departamento Jurídico Externo para sua Empresa', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus mollis, sem sagittis scelerisque pharetra, ligula diam vestibulum neque, ac feugiat felis lectus eget velit. Duis viverra rutrum arcu, id faucibus nibh porttitor a. Suspendisse sollicitudin nisi eget nunc interdum adipiscing. Nam imperdiet interdum faucibus. Sed id lectus vitae diam semper lacinia. Quisque vitae mi ac mi iaculis sagittis ullamcorper at ipsum.</span></p>'),
(6, 'Texto central', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ligula nibh, ornare non dapibus id, ultricies in est. Phasellus interdum orci ac diam');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `correspondentes`
--

CREATE TABLE IF NOT EXISTS `correspondentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(400) NOT NULL,
  `endereco` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `correspondentes`
--

INSERT INTO `correspondentes` (`id`, `nome`, `endereco`) VALUES
(1, 'Barbosa e Dias Advogados Associados', 'SRTVS quadra 701 torre 2 salas 501/9 - Brasília/DF');

-- --------------------------------------------------------

--
-- Estrutura da tabela `educacionais`
--

CREATE TABLE IF NOT EXISTS `educacionais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `educacionais`
--

INSERT INTO `educacionais` (`id`, `slug`, `titulo`, `texto`) VALUES
(1, 'apresentacao', 'Recuperação de Créditos Educacionais', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas dignissim diam sit amet ipsum fringilla non tempor purus fringilla. Aenean facilisis ligula sed diam ornare porta. Etiam blandit mauris sit amet nisl fringilla id commodo nisi adipiscing. Quisque tortor mauris, dictum nec tincidunt non, ultricies a ipsum. In vehicula eros eget eros sollicitudin scelerisque. Ut vitae est sed nisl elementum feugiat. Etiam accumsan nibh in erat imperdiet eget iaculis diam pretium. Nunc leo sem, laoreet in laoreet id, mattis vel sapien.</p>\n<p>Aenean ac libero vitae mauris mollis adipiscing. Donec vestibulum est in orci dapibus vitae semper neque semper. Cras suscipit nulla vitae ligula accumsan nec consequat lectus convallis. Integer placerat turpis eget turpis commodo porttitor. Pellentesque et mauris arcu, vel accumsan mi. Donec nec metus sit amet velit facilisis mattis. Morbi varius rhoncus dolor, sed elementum diam eleifend ac. Vivamus et leo mi. Integer mollis magna eu dolor sollicitudin convallis. Nam tortor lacus, bibendum euismod cursus nec, dignissim sit amet enim. Praesent non ultricies ante. Morbi at tempus leo. Duis luctus purus a sapien suscipit ut luctus urna lobortis. Cras tincidunt nisi lacinia tortor mollis eget vehicula enim tristique.</p>\n<p>Sed vulputate nisl eget massa pellentesque et luctus nisi hendrerit. Nulla facilisi. Ut varius, tortor sit amet semper interdum, libero nunc venenatis lacus, at egestas libero velit quis nunc. Duis sodales varius pellentesque. Nulla ultricies metus a arcu lobortis eu imperdiet augue pretium. Sed in porttitor metus. Proin volutpat, arcu nec suscipit semper, augue diam porttitor lorem, nec iaculis libero leo a nisl. In eget lorem metus. Aenean et mi nec neque gravida venenatis. Proin nec justo at diam sagittis scelerisque. Etiam cursus, massa at adipiscing feugiat, dui ipsum porta diam, ac varius nisi nisl ac turpis. Nullam interdum viverra nunc at adipiscing. Proin ligula nulla, tristique eu aliquet vel, malesuada eu risus. Integer enim sem, facilisis et vehicula in, rhoncus vel augue.</p>\n<p><strong>Nossas Metas</strong></p>\n<ul>\n<li>Meta 1&nbsp;</li>\n<li>Meta 2</li>\n<li>Meta 3</li>\n<li>Meta 4</li>\n<li>Meta 5</li>\n</ul>\n<p>&nbsp;</p>'),
(2, 'empresa', 'A Empresa', '<p>Prosperativa &eacute; um n&uacute;cleo de expertises que tem o prop&oacute;sito de contribuir com a Intelig&ecirc;ncia das empresas para que conquistem um novo patamar de prosperidade.</p>\n<p>Atuamos em conjunto com nossos clientes na conscientiza&ccedil;&atilde;o do seu momento atual e na transforma&ccedil;&atilde;o das suas organiza&ccedil;&otilde;es, para alcan&ccedil;ar a evolu&ccedil;&atilde;o dos seus neg&oacute;cios e dos resultados.</p>\n<p>Reconhecemos, respeitamos e valorizamos a hist&oacute;ria de cada um de nossos clientes e sua ess&ecirc;ncia que gerou e motivou o desenvolvimento do seu neg&oacute;cio. Sabemos que sem ela n&atilde;o seria poss&iacute;vel chegar ao momento atual, que exige mudan&ccedil;as!</p>\n<p>Contribu&iacute;mos com o desenvolvimento de vis&otilde;es estrat&eacute;gicas diferentes e, por que n&atilde;o, ousadas para gerar mais valor e promover a prosperidade. Para isso, precisamos de a&ccedil;&atilde;o, agilidade e criatividade na implementa&ccedil;&atilde;o das escolhas estrat&eacute;gicas, ou seja, transpira&ccedil;&atilde;o.</p>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresariais`
--

CREATE TABLE IF NOT EXISTS `empresariais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `empresariais`
--

INSERT INTO `empresariais` (`id`, `slug`, `titulo`, `texto`) VALUES
(1, 'apresentacao', 'Mensal', '<p><strong>Departamento Jur&iacute;dico Externo para sua Empresa</strong></p>\n<p>Lorem <span class="destaque">ipsum dolor</span> sit amet, consectetur adipiscing elit. Maecenas dignissim diam sit amet ipsum fringilla non tempor purus fringilla. Aenean facilisis ligula sed diam ornare porta. Etiam blandit mauris sit amet nisl fringilla id commodo nisi adipiscing. Quisque tortor mauris, dictum nec tincidunt non, ultricies a ipsum. In vehicula eros eget eros sollicitudin scelerisque. Ut vitae est sed nisl elementum feugiat. Etiam accumsan nibh in erat imperdiet eget iaculis diam pretium. Nunc leo sem, laoreet in laoreet id, mattis vel sapien.</p>\n<p>Aenean ac libero vitae mauris mollis adipiscing. Donec vestibulum est in orci dapibus vitae semper neque semper. Cras suscipit nulla vitae ligula accumsan nec consequat lectus convallis. Integer placerat turpis eget turpis commodo porttitor. Pellentesque et mauris arcu, vel accumsan mi. Donec nec metus sit amet velit facilisis mattis. Morbi varius rhoncus dolor, sed elementum diam eleifend ac. Vivamus et leo mi. Integer mollis magna eu dolor sollicitudin convallis. Nam tortor lacus, bibendum euismod cursus nec, dignissim sit amet enim. Praesent non ultricies ante. Morbi at tempus leo. Duis luctus purus a sapien suscipit ut luctus urna lobortis. Cras tincidunt nisi lacinia tortor mollis eget vehicula enim tristique.</p>\n<p>Sed vulputate nisl eget massa pellentesque et luctus nisi hendrerit. Nulla facilisi. Ut varius, tortor sit amet semper interdum, libero nunc venenatis lacus, at egestas libero velit quis nunc. Duis sodales varius pellentesque. Nulla ultricies metus a arcu lobortis eu imperdiet augue pretium. Sed in porttitor metus. Proin volutpat, arcu nec suscipit semper, augue diam porttitor lorem, nec iaculis libero leo a nisl. In eget lorem metus. Aenean et mi nec neque gravida venenatis. Proin nec justo at diam sagittis scelerisque. Etiam cursus, massa at adipiscing feugiat, dui ipsum porta diam, ac varius nisi nisl ac turpis. Nullam interdum viverra nunc at adipiscing. Proin ligula nulla, tristique eu aliquet vel, malesuada eu risus. Integer enim sem, facilisis et vehicula in, rhoncus vel augue.</p>'),
(2, 'empresa', 'Avulso', '<p>Prosperativa &eacute; um n&uacute;cleo de expertises que tem o prop&oacute;sito de contribuir com a Intelig&ecirc;ncia das empresas para que conquistem um novo patamar de prosperidade.</p>\n<p>Atuamos em conjunto com nossos clientes na conscientiza&ccedil;&atilde;o do seu momento atual e na transforma&ccedil;&atilde;o das suas organiza&ccedil;&otilde;es, para alcan&ccedil;ar a evolu&ccedil;&atilde;o dos seus neg&oacute;cios e dos resultados.</p>\n<p>Reconhecemos, respeitamos e valorizamos a hist&oacute;ria de cada um de nossos clientes e sua ess&ecirc;ncia que gerou e motivou o desenvolvimento do seu neg&oacute;cio. Sabemos que sem ela n&atilde;o seria poss&iacute;vel chegar ao momento atual, que exige mudan&ccedil;as!</p>\n<p>Contribu&iacute;mos com o desenvolvimento de vis&otilde;es estrat&eacute;gicas diferentes e, por que n&atilde;o, ousadas para gerar mais valor e promover a prosperidade. Para isso, precisamos de a&ccedil;&atilde;o, agilidade e criatividade na implementa&ccedil;&atilde;o das escolhas estrat&eacute;gicas, ou seja, transpira&ccedil;&atilde;o.</p>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `empresas`
--

INSERT INTO `empresas` (`id`, `slug`, `titulo`, `texto`) VALUES
(1, 'apresentacao', 'Apresentação', '<p align="JUSTIFY">A constante e permanente atualiza&ccedil;&atilde;o t&eacute;cnica, pessoal e intelectual dos profissionais e a possibilidade de contar com a colabora&ccedil;&atilde;o de correspondentes externos, tanto na &aacute;rea jur&iacute;dica quanto na empresarial e educacional, garantem aos clientes uma &oacute;tima e eficiente resposta aos seus interesses e objetivos.</p>\n<p align="JUSTIFY">O escrit&oacute;rio disp&otilde;e de ampla e moderna infra-estrutura, com excelente localiza&ccedil;&atilde;o al&eacute;m de equipamentos e softwares de &uacute;ltima gera&ccedil;&atilde;o para o atendimento pleno de seus clientes.</p>\n<h2 align="JUSTIFY">Vis&atilde;o</h2>\n<p>Prestar servi&ccedil;os jur&iacute;dicos e assessoria de excel&ecirc;ncia, com agilidade e segundo os mais elevados padr&otilde;es de qualidade. Possuir uma &oacute;tima equipe de profissionais atenciosos, capacitados e motivados. Acompanhar a evolu&ccedil;&atilde;o dos tempos com inova&ccedil;&atilde;o sem perder de vista os princ&iacute;pios a tradi&ccedil;&atilde;o e o respeito que aprendemos com gera&ccedil;&otilde;es passadas.</p>\n<p>O escrit&oacute;rio alia a criatividade &agrave; efic&aacute;cia, fatores determinantes nas solu&ccedil;&otilde;es satisfat&oacute;rias dos lit&iacute;gios bem como a sua preven&ccedil;&atilde;o, tendo ainda, como um de seus maiores diferenciais o atendimento personalizado, estando lado a lado com o cliente, o que permite o desenvolvimento de uma advocacia de ponta.</p>\n<h2>Miss&atilde;o</h2>\n<p>Conduzir nossos clientes aos seus objetivos jur&iacute;dicos e empresariais com rapidez atrav&eacute;s de orienta&ccedil;&otilde;es precisas, pareceres inovadores e medidas judiciais eficazes.</p>\n<p>Fazer valer a aplica&ccedil;&atilde;o das leis, dos direitos, princ&iacute;pios sempre em conson&acirc;ncia com os ditames da Constitui&ccedil;&atilde;o Federal, para a satisfa&ccedil;&atilde;o plena de nossos clientes &eacute; o compromisso deste escrit&oacute;rio.</p>\n<h2>Valores</h2>\n<ul>\n<li><span class="vermelho"><strong>&Eacute;tica acima de tudo</strong></span></li>\n</ul>\n<p style="margin: 0px 0px 10px 17px;" align="JUSTIFY">Atuar de forma &eacute;tica &eacute; ser &iacute;ntegro, transparente e respons&aacute;vel na rela&ccedil;&atilde;o com a verdade, com o cliente, e com o advogado que est&aacute; do outro lado da mesa.</p>\n<ul>\n<li><span class="vermelho"><strong>Qualidade &eacute; fazer bem feito</strong></span></li>\n</ul>\n<p style="margin: 0px 0px 10px 17px;" align="JUSTIFY">Nosso padr&atilde;o &eacute; dar o melhor atendimento, com clareza e objetividade.</p>\n<ul>\n<li><span class="vermelho"><strong>Atitude profissional</strong></span></li>\n</ul>\n<p style="margin: 0px 0px 10px 17px;" align="JUSTIFY">Importar-se verdadeiramente com o Cliente e seus interesses, servindo-o, com entusiasmo, para atingir seus resultados.</p>\n<ul>\n<li><span class="vermelho"><strong>Esp&iacute;rito de equipe</strong></span></li>\n</ul>\n<p style="margin: 0px 0px 10px 17px;" align="JUSTIFY">Juntos, cada um de n&oacute;s vale mais.</p>\n<ul>\n<li><span class="vermelho"><strong>Entusiasmo</strong></span></li>\n</ul>\n<p style="margin: 0px 0px 10px 17px;" align="JUSTIFY">Temos voca&ccedil;&atilde;o para a pr&aacute;tica da advocacia. Por isso a praticamos com dedica&ccedil;&atilde;o, prazer e alegria.</p>\n<ul>\n<li><span class="vermelho"><strong>Lealdade</strong></span></li>\n</ul>\n<p style="margin: 0px 0px 10px 17px;" align="JUSTIFY">Lealdade a si mesmo e ao outro, seja companheiro de trabalho, cliente ou oponente.</p>'),
(2, 'empresa', 'A Empresa', '<p>Prosperativa &eacute; um n&uacute;cleo de expertises que tem o prop&oacute;sito de contribuir com a Intelig&ecirc;ncia das empresas para que conquistem um novo patamar de prosperidade.</p>\n<p>Atuamos em conjunto com nossos clientes na conscientiza&ccedil;&atilde;o do seu momento atual e na transforma&ccedil;&atilde;o das suas organiza&ccedil;&otilde;es, para alcan&ccedil;ar a evolu&ccedil;&atilde;o dos seus neg&oacute;cios e dos resultados.</p>\n<p>Reconhecemos, respeitamos e valorizamos a hist&oacute;ria de cada um de nossos clientes e sua ess&ecirc;ncia que gerou e motivou o desenvolvimento do seu neg&oacute;cio. Sabemos que sem ela n&atilde;o seria poss&iacute;vel chegar ao momento atual, que exige mudan&ccedil;as!</p>\n<p>Contribu&iacute;mos com o desenvolvimento de vis&otilde;es estrat&eacute;gicas diferentes e, por que n&atilde;o, ousadas para gerar mais valor e promover a prosperidade. Para isso, precisamos de a&ccedil;&atilde;o, agilidade e criatividade na implementa&ccedil;&atilde;o das escolhas estrat&eacute;gicas, ou seja, transpira&ccedil;&atilde;o.</p>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `enderecos`
--

CREATE TABLE IF NOT EXISTS `enderecos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rua` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `telefone` varchar(255) NOT NULL,
  `telefone2` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cep` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `enderecos`
--

INSERT INTO `enderecos` (`id`, `rua`, `bairro`, `cidade`, `uf`, `telefone`, `telefone2`, `email`, `cep`) VALUES
(1, 'Rua Coronel Adriano Machado, 104', 'Chácara Santo Antonio', 'São Paulo', 'SP', '11 0000.0000', '11 00000.0000', 'diazrosa@diazroza.adv.br', '01250-050');

-- --------------------------------------------------------

--
-- Estrutura da tabela `eventos`
--

CREATE TABLE IF NOT EXISTS `eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `data` int(10) NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `eventos`
--

INSERT INTO `eventos` (`id`, `titulo`, `slug`, `data`, `texto`) VALUES
(7, 'Evento realizado', '', 1346727600, '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>'),
(9, 'Próximo evento 1', '', 1353549600, '<p><strong>Tema:</strong> Teste</p>\n<p><strong>Palestrante:</strong> Teste</p>\n<p><strong>Local:</strong> Teste</p>\n<p><strong>Inscri&ccedil;&otilde;es:</strong> <a href="http://oabsp.org.br" target="_blank">www.oabsp.org.br</a></p>'),
(10, 'Próximo evento 2', '', 1363230000, '<p><strong>Tema:</strong>&nbsp;Teste</p>\n<p><strong>Palestrante:</strong>&nbsp;Teste</p>\n<p><strong>Local:</strong>&nbsp;Teste</p>\n<p><strong>Inscri&ccedil;&otilde;es:</strong>&nbsp;<a href="http://oabsp.org.br/" target="_blank">www.oabsp.org.br</a></p>'),
(11, 'Evento realizado 2', '', 1351908000, '<p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></p>\n<p><span><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></span></p>'),
(12, 'Evento realizado 2', '', 1351908000, '<p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></p>\r\n<p><span><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></span></p>'),
(13, 'Evento realizado 2', '', 1351908000, '<p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></p>\r\n<p><span><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></span></p>'),
(14, 'Evento realizado 2', '', 1351908000, '<p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></p>\r\n<p><span><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></span></p>'),
(15, 'Evento realizado 2', '', 1351908000, '<p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></p>\r\n<p><span><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></span></p>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE IF NOT EXISTS `fotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `descricao` text NOT NULL,
  `imagem` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=114 ;

--
-- Extraindo dados da tabela `fotos`
--

INSERT INTO `fotos` (`id`, `titulo`, `descricao`, `imagem`) VALUES
(108, 'Título', '<p>Descri&ccedil;&atilde;o da foto com limite m&aacute;ximo de 260 caracteres</p>', 'cd6ae15a37c66c9c65d28b7ca6d6cdad.png'),
(109, 'Foto 2', '<p>Descri&ccedil;&atilde;o da foto 2</p>', '7189e182277ae8efda55e0111ec4233e.jpg'),
(110, 'Foto 3', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus&nbsp;Cum sociis natoque penatibus &nbsp;Cum sociis natoque penatibus &nbsp;Cum sociis natoque penatibus &nbsp;Cum sociis natoque penatibus &nbsp;</p>', '8f0af60174ad00519f733758cebd82b1.jpg'),
(111, 'Foto 4', '<p>Descri&ccedil;&atilde;o foto 4</p>', '32e24cbf203bdb5621d3faf920986198.jpg'),
(112, 'Foto 5', '<p>Descri&ccedil;&atilde;o foto 5</p>', '2b1ab08eeca9269de1d14b126a7e9cb8.jpg'),
(113, 'Foto 6', '<p>Descri&ccedil;&atilde;o foto 6</p>', 'e884d7f56a08720000c634d7311f9e43.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `integrantes`
--

CREATE TABLE IF NOT EXISTS `integrantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `integrantes`
--

INSERT INTO `integrantes` (`id`, `slug`, `foto`, `nome`, `texto`) VALUES
(9, '', '11.jpg', 'KLEBER GRASSO RODRIGUES', '<p>rofissional com mais de 30 anos de experi&ecirc;ncia em processos que englobam planejamento estrat&eacute;gico, comercializa&ccedil;&atilde;o, intelig&ecirc;ncia de mercado, negocia&ccedil;&atilde;o e administra&ccedil;&atilde;o de contratos de desenvolvimento e fornecimento de bens e servi&ccedil;os de alta tecnologia, no Brasil e exterior, bem como servi&ccedil;os de consultoria e treinamento em Gest&atilde;o da Qualidade, Excel&ecirc;ncia no Atendimento a Clientes e Gest&atilde;o de Processos para organiza&ccedil;&otilde;es de pequeno, m&eacute;dio e grande porte, dos setores da ind&uacute;stria e de servi&ccedil;os.</p>\n<p>Graduado em Engenharia (ITA - SJCampos); p&oacute;s-gradua&ccedil;&atilde;o em Administra&ccedil;&atilde;o de Ci&ecirc;ncia e Tecnologia (ITA- SJCampos) e Din&acirc;mica dos Grupos (SBDG &ndash; S&atilde;o Paulo); especializa&ccedil;&otilde;es em Garantia da Qualidade na Empresa Privada (Roma/It&aacute;lia), Administra&ccedil;&atilde;o P&uacute;blica e Privada (UNIFA - Rio de Janeiro), Treinamento e Processo de Desenvolvimento Organizacional (Casa do Psic&oacute;logo - S&atilde;o Paulo), Banca Examinadora do Pr&ecirc;mio Nacional da Qualidade (FPNQ), Programa SEBRAE para Qualidade nas MPEs (SEBRAE &ndash; MG), Psicodin&acirc;mica Aplicada a Neg&oacute;cios (Render Capacita&ccedil;&atilde;o &ndash; S&atilde;o Paulo), The COACHING CLINIC&reg; (Corporate Coach U/IDPH - S&atilde;o Paulo) e O Pensar e T&eacute;cnicas Sist&ecirc;micas (Alicio Gobis &ndash; S&atilde;o Paulo).</p>'),
(10, '', '', 'MARCIA SILVEIRA GUIMARÃES', '<p>Profissional Com 9 Anos De Experi&ecirc;ncia Na Forma&ccedil;&atilde;o E Capacita&ccedil;&atilde;o De Equipes Em Empresas De Pequeno E M&eacute;dio Porte.</p>\n<p>Psic&oacute;loga Com Experi&ecirc;ncia Cl&iacute;nica E Educacional Durante 15 Anos.</p>\n<p>Empres&aacute;ria Com Mais De 10 Anos De Atua&ccedil;&atilde;o No Segmento De Prospec&ccedil;&atilde;o E Comercializa&ccedil;&atilde;o De Treinamentos Na &Aacute;rea De Desenvolvimento Empresarial E Humano.</p>\n<p>Forma&ccedil;&atilde;o Em Psicologia (PUC-SP) E Especializa&ccedil;&otilde;es Em: Psicodrama Aplicado (ABPS-SP), Psicodin&acirc;mica Aplicada A Neg&oacute;cios (Render Capacita&ccedil;&atilde;o - S&atilde;o Paulo), The Coaching Clinic&reg; (Corporate Coach U/Idph - S&atilde;o Paulo) E Gest&atilde;o Estrat&eacute;gica (Render Capacita&ccedil;&atilde;o - S&atilde;o Paulo).</p>\n<p>Mba Em Rh Com &Ecirc;nfase Em Consultoria Interna E Desenvolvimento Organizacional &ndash; Bi Internacional</p>'),
(11, '', '', 'ROGÉRIO SILVEIRA MONTEIRO', '<p>Mais de 12 anos de experi&ecirc;ncia em desenvolvimento estrat&eacute;gico de neg&oacute;cios em empresas de diversos segmentos de mercado e portes.</p>\n<p>Desenvolve projetos de consultoria nas &aacute;reas de planejamento estrat&eacute;gico, marketing, vendas, financeira e pessoas, com &ecirc;nfase na busca de resultados sustent&aacute;veis por meio da implementa&ccedil;&atilde;o de ferramentas de gest&atilde;o e integra&ccedil;&atilde;o das equipes nas organiza&ccedil;&otilde;es. Atua como Coach nos ambientes profissionais e pessoais. Dirige projetos de turnaround management com &ecirc;nfase na recupera&ccedil;&atilde;o de valor.</p>\n<p>Experi&ecirc;ncia docente no Curso de P&oacute;s-Gradua&ccedil;&atilde;o em Gest&atilde;o Estrat&eacute;gica de Pessoas na Universidade Mackenzie e de pesquisa junto ao N&uacute;cleo de Gest&atilde;o Baseada em Valores da mesma universidade.</p>\n<p>Mestre em Administra&ccedil;&atilde;o (Mackenzie-SP), MBA em Marketing (USP), P&oacute;s-graduado em Controladoria (FAAP) e Graduado em Economia (PUC-SP). Forma&ccedil;&atilde;o em Din&acirc;mica dos Grupos (SBDG &ndash; SP); Master Coach Instituto Brasileiro de Coaching (IBC); Behaviour Analyst pelo Global Coaching Community.</p>\n<p>Membro fundador da TMA Brasil &ndash; Turnaround Management Association.</p>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `links`
--

INSERT INTO `links` (`id`, `titulo`, `link`) VALUES
(1, 'Supremo Tribunal Federal', 'http://stf.gov.br'),
(2, 'Edição', 'http://stj.gov.br'),
(4, 'Supremo Tribunal Federal', 'http://stf.gov.br'),
(7, 'Teste de edição', 'http://example.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noticia_titulo` varchar(255) NOT NULL,
  `noticia_conteudo` text NOT NULL,
  `noticia_imagem` varchar(255) NOT NULL,
  `noticia_data_publicacao` int(10) NOT NULL,
  `noticia_resumo` varchar(400) NOT NULL,
  `noticia_categoria` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id`, `noticia_titulo`, `noticia_conteudo`, `noticia_imagem`, `noticia_data_publicacao`, `noticia_resumo`, `noticia_categoria`) VALUES
(15, 'Obra | Artigo + Imagem', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>', '0001.png', 1351735200, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'obras-artigos'),
(16, 'Reportagem concedida + Imagem', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n<p><a href="http://trupe.net" target="_blank">Veja a reportagem completa</a></p>', '000.png', 1349060400, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'reportagens-concedidas'),
(17, 'Obra | Artigo', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>', '', 1346641200, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'obras-artigos'),
(18, 'Notícia', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>', '', 1338865200, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'noticias'),
(19, 'Notícia + Imagem < 200px', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.vLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>', '80x800.png', 1337137200, 'As imagens cadastradas junto às notícias não devem ter largura inferior a 200px ou as miniaturas ficarão distorcidas', 'noticias'),
(20, 'Reportagem concedida', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;</p>\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.v&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>', '', 1352253600, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'reportagens-concedidas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE IF NOT EXISTS `paginas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `paginas`
--

INSERT INTO `paginas` (`id`, `slug`, `titulo`, `texto`) VALUES
(1, 'apresentacao', 'Apresentação', '<p><span style="font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>\n<p><span style="font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>\n<p><span style="font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>\n<p><span style="font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>\n<p>&nbsp;</p>'),
(2, 'empresa', 'A Empresa', '<p>Prosperativa &eacute; um n&uacute;cleo de expertises que tem o prop&oacute;sito de contribuir com a Intelig&ecirc;ncia das empresas para que conquistem um novo patamar de prosperidade.</p>\n<p>Atuamos em conjunto com nossos clientes na conscientiza&ccedil;&atilde;o do seu momento atual e na transforma&ccedil;&atilde;o das suas organiza&ccedil;&otilde;es, para alcan&ccedil;ar a evolu&ccedil;&atilde;o dos seus neg&oacute;cios e dos resultados.</p>\n<p>Reconhecemos, respeitamos e valorizamos a hist&oacute;ria de cada um de nossos clientes e sua ess&ecirc;ncia que gerou e motivou o desenvolvimento do seu neg&oacute;cio. Sabemos que sem ela n&atilde;o seria poss&iacute;vel chegar ao momento atual, que exige mudan&ccedil;as!</p>\n<p>Contribu&iacute;mos com o desenvolvimento de vis&otilde;es estrat&eacute;gicas diferentes e, por que n&atilde;o, ousadas para gerar mais valor e promover a prosperidade. Para isso, precisamos de a&ccedil;&atilde;o, agilidade e criatividade na implementa&ccedil;&atilde;o das escolhas estrat&eacute;gicas, ou seja, transpira&ccedil;&atilde;o.</p>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `parceiros`
--

CREATE TABLE IF NOT EXISTS `parceiros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Extraindo dados da tabela `parceiros`
--

INSERT INTO `parceiros` (`id`, `nome`, `imagem`, `texto`, `link`) VALUES
(7, 'Lorem ipsum', 'parceiro.png', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>', 'http://prosperativa.dev'),
(8, 'Lorem Ipsum', 'parceiro.png', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>', 'http://trupe.net'),
(9, 'Lorem ipsum', 'parceiro.png', '<p>\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\r\n</p>', 'http://trupe.net'),
(16, 'Lorem Ipsum', 'parceiro.png', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>', 'http://trupe.net'),
(17, 'Lorem ipsum', 'parceiro.png', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>', 'http://trupe.net'),
(18, 'Lorem Ipsum', 'parceiro.png', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>', 'http://trupe.net'),
(19, 'Lorem ipsum', 'parceiro.png', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>', 'http://trupe.net'),
(20, 'Lorem Ipsum', 'parceiro.png', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>', 'http://trupe.net'),
(21, 'Lorem ipsum', 'parceiro.png', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>', 'http://trupe.net');

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  `default` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `role`, `default`) VALUES
(1, 'admin', 0),
(2, 'user', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `texto` text NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `padrao` tinyint(1) NOT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `slides`
--

INSERT INTO `slides` (`id`, `titulo`, `link`, `texto`, `imagem`, `padrao`, `ordem`, `created`, `updated`) VALUES
(6, 'Slide teste 1', 'http://link1.com', '<p>A experi&ecirc;ncia de uma d&eacute;cada da Diaz Rosa Advogados no relacionamento com os diretores, pedagogos e respons&aacute;veis financeiros pelos alunos, uma equipe especializada e bem treinada conjuntamente com o dom&iacute;nio da espec&iacute;fica legisla&ccedil;&atilde;o do setor juspedag&oacute;gico, faz com que nosso escrit&oacute;rio alcance os melhores resultados na recupera&ccedil;&atilde;o de cr&eacute;ditos educacionais, reduzindo sistematicamente os &iacute;ndices de inadimpl&ecirc;ncia das institui&ccedil;&otilde;es de ensino preservando a rela&ccedil;&atilde;o das institui&ccedil;&otilde;es com seus alunos.</p>', 'home-slider11.jpg', 0, 2, 0, 0),
(9, 'Recuperação de Créditos Educacionais', 'http://trupe.net', '<p>A experi&ecirc;ncia de uma d&eacute;cada da Diaz Rosa Advogados no relacionamento com os diretores, pedagogos e respons&aacute;veis financeiros pelos alunos, uma equipe especializada e bem treinada conjuntamente com o dom&iacute;nio da espec&iacute;fica legisla&ccedil;&atilde;o do setor juspedag&oacute;gico, faz com que nosso escrit&oacute;rio alcance os melhores resultados na recupera&ccedil;&atilde;o de cr&eacute;ditos educacionais, reduzindo sistematicamente os &iacute;ndices de inadimpl&ecirc;ncia das institui&ccedil;&otilde;es de ensino preservando a rela&ccedil;&atilde;o das institui&ccedil;&otilde;es com seus alunos</p>', 'home-slider1.jpg', 0, 1, 0, 0),
(10, 'Teste', 'http://teste.com', '<p>A experi&ecirc;ncia de uma d&eacute;cada da Diaz Rosa Advogados no relacionamento com os diretores, pedagogos e respons&aacute;veis financeiros pelos alunos, uma equipe especializada e bem treinada conjuntamente com o dom&iacute;nio da espec&iacute;fica legisla&ccedil;&atilde;o do setor juspedag&oacute;gico, faz com que nosso escrit&oacute;rio alcance os melhores resultados na recupera&ccedil;&atilde;o de cr&eacute;ditos educacionais, reduzindo sistematicamente os &iacute;ndices de inadimpl&ecirc;ncia das institui&ccedil;&otilde;es de ensino preservando a rela&ccedil;&atilde;o das institui&ccedil;&otilde;es com seus alunos.</p>', 'home-slider12.jpg', 0, 3, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `role_id` int(11) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role_id`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 'trupe', '$P$BtiANtNUrn3qm2BjBTdR37M2VtSnqE0', 'nilton@trupe.net', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2013-05-16 13:58:33', '2012-10-18 16:47:07', '2013-05-16 13:58:33'),
(3, 'diazrosa', '$P$BIg5FJg/59szkiBCRQ7czGIBXil1ad0', 'niltondefreitas@gmail.com', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '0000-00-00 00:00:00', '2013-05-17 17:58:26', '2013-05-17 18:00:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
