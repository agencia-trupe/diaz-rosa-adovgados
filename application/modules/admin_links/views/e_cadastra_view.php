<div class="span9">

<legend><?php echo $acao == 'editar' ? 'Editar link' : 'Novo link' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php echo form_open_multipart(($acao == 'editar') ? 'painel/links/atualiza' : 'painel/links/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php echo ($acao == 'edita') ? form_hidden('acao', 'edita') : form_hidden('acao', 'cadastra'); ?>  
    </div>
    <?php if($acao == 'editar') : ?>
      <?php echo form_hidden('id', set_value('id', $link->id)); ?>
    <?php endif; ?>
  <div class="row-fluid">
      <div class="span11">
          <label for="titulo">Titulo</label>
              <?php echo form_input(array('name'=>'titulo', 'id' => 'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $link->titulo : ''), 'class' => 'span11',)); ?>
              <?php echo form_error('titulo'); ?>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="link">Link</label>
            <div class="controls">
              <?php echo form_input('link', set_value('link', $acao == 'editar' ? $link->link : ''), 'class="span11"'); ?>
              <span class="help-inline"><?php echo form_error('link'); ?></span>
            </div>
      </div>
     </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>