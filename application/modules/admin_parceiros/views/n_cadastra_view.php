<div class="span12">

<legend><?php echo $acao == 'editar' ? 'Editar parceiro' : 'Nova parceiro' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php

    $categoria = array(
        'parceiros' => 'Notícias',
        'obras-artigos' => 'Obras | Artigos',
        'reportagens-concedidas' => 'Reportagens concedidas',
    );
    echo form_open_multipart(($acao == 'editar') ? 'painel/parceiros/atualiza' : 'painel/parceiros/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php if($acao == 'edita')
      {
        echo form_hidden('acao', 'edita');
      }
      else{
        echo form_hidden('acao', 'cadastra');
      }
      ?>  
      </div>
    <?php if($acao == 'editar') : ?>
    <?php echo
    form_hidden('id', set_value('id', $parceiro->id));
    ?>
  <?php endif; ?>
    <div class="row-fluid">
        <div class="span9">
            <label for="">Nome</label>
            <?php echo form_input(array('name'=>'nome', 'id' => 'nome', 'value'=>set_value('nome', $acao == 'editar' ? $parceiro->nome : ''), 'class' => 'span12',)); ?>
            <?php echo form_error('nome'); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span9">
            <label for="link">Link</label>
            <?php echo form_input(array('name'=>'link', 'id' => 'link', 'value'=>set_value('link', $acao == 'editar' ? $parceiro->link : ''), 'class' => 'span12',)); ?>
            <?php echo form_error('link'); ?>
        </div>
    </div>
    <div class="row-fluid">
    </div>
    <br>
  <div class="row-fluid">
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="texto">Conteúdo</label>
            <div class="controls">
              <?php echo form_textarea('texto', set_value('texto', $acao == 'editar' ? $parceiro->texto : ''), 'class="tinymce span11"'); ?>
              <span class="help-inline"><?php echo form_error('texto'); ?></span>
            </div>
      </div>
     <?php if($acao == 'editar'): ?>
     <?php if($parceiro->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/parceiros/<?php echo $parceiro->imagem; ?>" alt="<?php echo $parceiro->nome; ?>" >
     <?php endif; ?>
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ($acao == 'editar' ? 'Adicionar / Alterar Imagem' : 'Imagem'); ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem'), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
  </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>