<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <legend>Parceiros <?php echo anchor('painel/parceiros/cadastra', 'Cadastrar', 'class="btn"'); ?></legend>
        </div>
    </div>
  <?php if(isset($result)): ?>
     <?php
        
            $tmpl = array (
                                'table_open'          => '<table class="table table-striped table-hover">',

                                'heading_row_start'   => '<tr>',
                                'heading_row_end'     => '</tr>',
                                'heading_cell_start'  => '<th>',
                                'heading_cell_end'    => '</th>',

                                'row_start'           => '<tr>',
                                'row_end'             => '</tr>',
                                'cell_start'          => '<td>',
                                'cell_end'            => '</td>',


                                'table_close'         => '</table>'
                        );

            $this->table->set_template($tmpl); 
            $this->table->set_heading(array('Imagem', 'Nome', 'Texto', 'Ações'));
            foreach ($result as $item)
            {
            $imagem = '<img src="' . base_url() . 'assets/img/parceiros/' . $item->imagem . '">';
            $nome = $item->nome;
            $texto = $item->texto;
            $acoes = anchor('painel/parceiros/editar/' . $item->id, 'Editar', 'class="btn btn-small btn-warning"') . anchor('painel/parceiros/apaga/' . $item->id, 'Remover', array('id' => 'removelink', 'class' => 'btn btn-mini btn-danger')) ; 
            $this->table->add_row(array($imagem, $nome, $texto, $acoes));
            }
            echo $this->table->generate();

            ?> 
  <?php endif; ?>
        

</div><!--/span-->