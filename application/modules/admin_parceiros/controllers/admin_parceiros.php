<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class admin_parceiros extends MX_Controller
{
    function index(){

        $this->lista();
    }
    /**
    *Lista todos os parceiros cadastrados atualmente
    *
    * @return [type] [description]
    */
    function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Prosperativa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
          {
            $this->load->library('pagination');
            $this->load->library('table');
            $pagination_config = array(
                        'base_url'       => site_url() . 'admin_parceiros/lista/',
                        'total_rows'     => $this->db->get('parceiros')->num_rows(),
                        'per_page'       => 30,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
                $this->pagination->initialize($pagination_config);
                //Obtendo resultados no banco
                $this->load->model('parceiros/parceiro');
                $data['result'] = $this->parceiro->
                get_all_all($pagination_config['per_page'], $this->uri->
                    segment(3));

                $data['title'] = 'Prosperativa - parceiros - Lista';
                $data['module'] = 'parceiros';
                $data['main_content'] = 'n_lista_view';
                $this->load->view('includes/template', $data);
          }
          else
          {
          Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de 
            acesso area administrativa sem privilégios');
          $this->session->set_flashdata('error', 'Erro de permissão. 
                                                Você precisa ser 
                                                administrador para realizar essa ação');
          redirect();

          } 
        }
    }
    /**
     * Mostra a página de edição de um parceiro cujo id foi passado como 
     * parâmetro.
     *
     * @param  [int] $id [description]
     * @return [mixed]     [description]
     */
    function editar($id)
    {
        /**
         * Verifica se o usuário está logado para então prosseguir ou não.
         */
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Prosperativa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            //Verifica se o usuário tem nível de acesso permitido
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $id = $this->uri->segment(4);
                if(!$id)
                {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                    redirect('painel/parceiros/lista');
                }
                else
                {
                    $this->load->model('parceiros/parceiro');
                    $data['module'] = 'parceiros';
                    $data['title'] = 'Prosperativa - parceiros - Editar';

                    if($this->parceiro->get_parceiro($id))
                    {
                        $data['parceiro'] = $this->parceiro->get_parceiro($id);
                        $data['acao'] = 'editar';
                        $data['main_content'] = 'n_cadastra_view';
                        $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('parceiros/lista');
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();
            }
        }
    }

    function cadastra()
    {

        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Prosperativa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
              if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
              {
                  $data['title'] = 'Prosperativa - parceiros - Novo parceiro';
                  $data['module'] = 'parceiros';
                  $data['acao'] = 'cadastra';
                  $data['main_content'] = 'n_cadastra_view';
                  $this->load->view('includes/template', $data);
              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão.
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function salva(){

        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Prosperativa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            $config = array(
                array(
                    'field' => 'nome',
                    'label' => 'nome',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'texto',
                    'label' => 'texto',
                    'rules' => 'required',
                ),
                 array(
                    'field' => 'link',
                    'label' => 'link',
                    'rules' => 'required',
                ),
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');
            $data['acao'] = $this->input->post('acao');

            if($this->form_validation->run() == FALSE ){
                  $data['title'] = 'Prosperativa - parceiros - Nova parceiro';
                  $data['module'] = 'parceiros';
                  $data['main_content'] = 'n_cadastra_view';
                  $this->load->view('includes/template', $data);
            }
            else
            {   //Verifica se foi feito o upload de uma imagem
                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    $config['upload_path'] = './assets/img/parceiros/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2000';
                    $config['max_width']  = '1600';
                    $config['max_height']  = '1200';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {
                            $data['error'] = array('error' => $this->upload->display_errors());

                            $data['title'] = 'Prosperativa - parceiros - Nova parceiro';
                            $data['module'] = 'parceiros';
                            $data['main_content'] = 'n_cadastra_view';
                            $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                        if(
                            $this->image_moo->load($file_uploaded)
                                ->resize(600,600,$pad=FALSE)
                                ->save($new_file,true)
                            
                            &&

                            $this->image_moo->load($new_file)
                                 ->resize_crop(200,120)
                                 ->save($thumb, true)
                            )
                        {
                             $this->load->model('parceiros/parceiro');

                            //prepara o array com os dados para enviar ao model
                            $dados = array(
                                    'imagem' => $upload_data['file_name'],
                                    'nome' => $this->input->post('nome'),
                                    'texto' => $this->input->post('texto'),
                                    'link' => $this->input->post('link'),
                            );

                            if( ! $this->parceiro->cadastra_imagem($dados))
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/parceiros/cadastra');
                            }
                            else
                            {
                                $this->session->set_flashdata('success', 'parceiro cadastrada
                                com sucesso!');
                                redirect('painel/parceiros/lista');
                            }
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('painel/parceiros/cadastra');
                        }
                    }
                }
                else
                {
                    $this->load->model('parceiros/parceiro');

                    //prepara o array com os dados para enviar ao model
                    $dados = array(
                            'nome' => $this->input->post('nome'),
                            'texto' => $this->input->post('texto'),
                        );

                    if( ! $this->parceiro->cadastra($dados))
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('painel/parceiros/cadastra');
                    }
                    else
                    {
                        $this->session->set_flashdata('success', 'parceiro cadastrada
                        com sucesso!');
                        redirect('painel/parceiros/lista');
                    }
                }
            }
        }
    }

    function atualiza()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Prosperativa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $config = array(
                    array(
                    'field' => 'nome',
                    'label' => 'nome',
                    'rules' => 'required',
                    ),
                    array(
                        'field' => 'texto',
                        'label' => 'texto',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'link',
                        'label' => 'link',
                        'rules' => 'required',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                if($this->form_validation->run() == FALSE )
                {
                    $id  = $this->input->post('id');
                    $this->load->model('parceiros/parceiro');
                    $data['module'] = 'parceiros';
                    $data['title'] = 'Prosperativa - parceiros - Editar';
                    $data['parceiro'] = $this->parceiro->get_parceiro($id);
                    $data['acao'] = 'editar';
                    $data['main_content'] = 'n_cadastra_view';
                    $this->load->view('includes/template', $data);
                }
                else
                {
                    //verifica se foi postada uma imagem
                    if(strlen($_FILES["imagem"]["name"])>0)
                    {
                        $config['upload_path'] = './assets/img/parceiros/';
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = '2000';
                        $config['max_width']  = '1600';
                        $config['max_height']  = '1200';

                        $this->load->library('upload', $config);

                        if ( ! $this->upload->do_upload('imagem'))
                        {
                                $data['error'] = array('error' => $this->upload->display_errors());

                                $id  = $this->input->post('id');
                                $this->load->model('parceiros/parceiro');
                                $data['module'] = 'parceiros';
                                $data['title'] = 'Prosperativa - parceiros - Editar';
                                $data['parceiro'] = $this->parceiro->get_parceiro($id);
                                $data['acao'] = 'editar';
                                $data['main_content'] = 'n_cadastra_view';
                                $this->load->view('includes/template', $data);
                        }
                        else
                        {
                            $this->load->library('image_moo');
                            //Is only one file uploaded so it ok to use it with $uploader_response[0].
                            $upload_data = $this->upload->data();
                            $file_uploaded = $upload_data['full_path'];
                            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                            $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                            if(
                                $this->image_moo->load($file_uploaded)
                                    ->resize(600,600,$pad=FALSE)
                                    ->save($new_file,true)
                                
                                &&

                                $this->image_moo->load($new_file)
                                     ->resize_crop(200,120)
                                     ->save($thumb, true)
                                )
                            {
                                 $this->load->model('parceiros/parceiro');

                                //prepara o array com os dados para enviar ao model
                                $dados = array(
                                        'id'     => $this->input->post('id'),
                                        'nome' => $this->input->post('nome'),
                                        'imagem' => $upload_data['file_name'],
                                        'texto' => $this->input->post('texto'),
                                        'link' => $this->input->post('link'),
                                    );

                                if( ! $this->parceiro->atualiza_imagem($dados))
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/parceiros/cadastra');
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'parceiro cadastrada
                                    com sucesso!');
                                    redirect('painel/parceiros/lista');
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/parceiros/cadastra');
                            }
                        }
                    }
                    else
                    {

                        $this->load->model('parceiros/parceiro');

                        //prepara o array com os dados para enviar ao model
                        $dados = array(
                                    'id' => $this->input->post('id'),
                                    'nome' => $this->input->post('nome'),
                                    'texto' => $this->input->post('texto'),
                                    'link' => $this->input->post('link'),
                            );


                            if( ! $this->parceiro->atualiza($dados))
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/parceiros/atualiza/' . $this->input->post('id'));
                            }
                            else
                            {
                                $this->session->set_flashdata('success', 'parceiro atualizado
                                com sucesso!');
                                redirect('painel/parceiros/lista');
                            }
                    }
                }

              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function apaga($id)
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Prosperativa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $id = $this->uri->segment(4);
                if(!$id)
                {
                $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                redirect('painel/parceiros/lista');
                }
                else
                {
                    $this->load->model('parceiros/parceiro');
                    if($this->parceiro->delete_parceiro($id))
                    {
                        $this->session->set_flashdata('success', 'Registro apagado
                        com sucesso');
                         redirect('painel/parceiros/lista');
                    }
                     else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                         realizada, tente novamente ou entre em contado com o suporte');
                        redirect('painel/parceiros/lista');
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();

            }
        }

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */