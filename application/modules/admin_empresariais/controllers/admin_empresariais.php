<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_empresariais extends MX_Controller
{
    function index(){

            $this->lista();
        }
        /**
         *Lista todos os empresariais cadastrados atualmente
         *
         * @return [type] [description]
         */
    function lista()
    {
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',
                    $this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
              {
                    $this->load->library('table');
                    $this->load->model('empresariais/empresarial');
                    $data['result'] = $this->empresarial->
                    get_all();

                    $data['title'] = 'Prosperativa - Setor Educacional';
                    $data['module'] = 'empresarial';
                    $data['main_content'] = 'e_lista_view';
                    $this->load->view('includes/template', $data);
              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de 
                acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser 
                                                    administrador para realizar essa ação');
              redirect();

              } 
            }
    }
        /**
         * Mostra a página de edição de um empresarial cujo id foi passado como 
         * parâmetro.
         *
         * @param  [int] $id [description]
         * @return [mixed]     [description]
         */
        function editar($id)
        {
            /**
             * Verifica se o usuário está logado para então prosseguir ou não.
             */
            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                //Verifica se o usuário tem nível de acesso permitido
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $id = $this->uri->segment(4);
                    if(!$id)
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('painel/empresarial/lista');
                    }
                    else
                    {
                        $this->load->model('empresariais/empresarial');
                        $data['module'] = 'empresarial';
                        $data['title'] = 'Prosperativa - empresariais - Editar';

                        
                        $data['integrante'] = $this->empresarial->get_conteudo($id);
                        $data['acao'] = 'editar';
                        $data['main_content'] = 'e_cadastra_view';
                        $this->load->view('includes/template', $data);
                    }
                }
                else
                {
                    Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                    $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                    redirect();
                }
            }
        }

        function cadastra()
        {

            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                  if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                  {
                      $data['title'] = 'Prosperativa';
                      $data['module'] = 'empresarial';
                      $data['acao'] = 'cadastra';
                      $data['main_content'] = 'e_cadastra_view';
                      $this->load->view('includes/template', $data);
                  }
                  else
                  {
                  Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                  $this->session->set_flashdata('error', 'Erro de permissão.
                                                        Você precisa ser administrador para realizar essa ação');
                  redirect();

                  }
            }
        }

        function salva(){

            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                $config = array(
                    array(
                        'field' => 'titulo',
                        'label' => 'titulo',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'texto',
                        'label' => 'texto',
                        'rules' => 'required',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');
                $data['acao'] = $this->input->post('acao');

                if($this->form_validation->run() == FALSE ){
                      $data['title'] = 'Prosperativa - empresariais - Novo empresarial';
                      $data['module'] = 'empresarial';
                      $data['acao'] = 'cadastra';
                      $data['main_content'] = 'e_cadastra_view';
                      $this->load->view('includes/template', $data);
                }
                else
                {   //Verifica se foi feito o upload de uma imagem
                    $this->load->model('empresariais/empresarial');

                    //prepara o array com os dados para enviar ao model
                    $dados = array(
                            'titulo' => $this->input->post('titulo'),
                            'texto' => $this->input->post('texto'),
                        );

                    if( ! $this->empresarial->cadastra($dados))
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('painel/empresarial/cadastra');
                    }
                    else
                    {
                        $this->session->set_flashdata('success', 'Integrante cadastrado
                        com sucesso!');
                        redirect('painel/empresarial/lista');
                    }
                    
                }
            }
        }

        function atualiza()
        {
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $config = array(
                        array(
                            'field' => 'titulo',
                            'label' => 'titulo',
                            'rules' => 'required',
                        ),
                        array(
                            'field' => 'texto',
                            'label' => 'texto',
                            'rules' => 'required',
                        ),
                    );
                    $this->load->library('form_validation');
                    $this->form_validation->set_rules($config);
                    $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                    if($this->form_validation->run() == FALSE )
                    {
                        $id  = $this->input->post('id');
                        $data['integrante'] = $this->empresarial->get_conteudo($id);
                        $data['acao'] = 'editar';
                        $data['main_content'] = 'e_cadastra_view';
                        $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        //verifica se foi postada uma imagem
                        
                        $this->load->model('empresariais/empresarial');

                        //prepara o array com os dados para enviar ao model
                        $dados = array(
                               'id' => $this->input->post('id'),
                                'titulo' => $this->input->post('titulo'),
                                'texto' => $this->input->post('texto'),
                            );


                            if( ! $this->empresarial->atualiza($dados))
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/empresarial/atualiza/' . $this->input->post('id'));
                            }
                            else
                            {
                                $this->session->set_flashdata('success', 'Registro alterado
                                com sucesso!');
                                redirect('painel/empresarial/lista');
                            }
                        
                    }

                  }
                  else
                  {
                  Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                  $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                  redirect();

                  }
            }
        }

        function apaga($id)
        {
            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $id = $this->uri->segment(4);
                    if(!$id)
                    {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                    redirect('painel/empresarial/lista');
                    }
                    else
                    {
                        $this->load->model('empresariais/empresarial');
                        if($this->empresarial->delete_empresarial($id))
                        {
                            $this->session->set_flashdata('success', 'Registro apagado
                            com sucesso');
                             redirect('painel/empresarial/lista');
                        }
                         else
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                             realizada, tente novamente ou entre em contado com o suporte');
                            redirect('painel/empresarial/lista');
                        }
                    }
                }
                else
                {
                    Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                    $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                    redirect();

                }
            }

        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */