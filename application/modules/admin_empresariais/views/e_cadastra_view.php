<div class="span9">

<legend><?php echo $acao == 'editar' ? 'Editar Seção' : 'Nova Seção' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php echo form_open_multipart(($acao == 'editar') ? 'painel/empresarial/atualiza' : 'painel/empresarial/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php echo ($acao == 'edita') ? form_hidden('acao', 'edita') : form_hidden('acao', 'cadastra'); ?>  
    </div>
    <?php if($acao == 'editar') : ?>
      <?php echo form_hidden('id', set_value('id', $integrante->id)); ?>
    <?php endif; ?>
  <div class="row-fluid">
      <!--Título-->
      <div class="span11">
          <label for="titulo">Título</label>
              <?php echo form_input(array('name'=>'titulo', 'id' => 'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $integrante->titulo : ''), 'class' => 'span11',)); ?>
              <?php echo form_error('titulo'); ?>
      </div>
    </div>
    <div class="row-fluid">
      <!--fim de Título-->
      <div class="span11">
            <label for="texto">Texto</label>
              <?php echo form_textarea('texto', set_value('texto', $acao == 'editar' ? $integrante->texto : ''), 'class="tinymce span7"'); ?>
              <?php echo form_error('texto'); ?>
              <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
     </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>