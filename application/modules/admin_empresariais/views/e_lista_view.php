<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <legend>Consultoria Jurídica Empresarial <?php echo anchor('painel/empresarial/cadastra', 'Cadastrar', 'class="btn"'); ?></legend>
        </div>
        <?php 
            if (isset($update_success))
            {
                echo '<div class="span8"><div class="alert alert-success">'.$update_success.$today.'às '.$time.'</div></div>';
            }
        ?>
    </div>
  <?php if(isset($result)): ?>
     <?php
        
            $tmpl = array (
                                'table_open'          => '<table class="table table-striped table-bordered table-condensed">',

                                'heading_row_start'   => '<tr>',
                                'heading_row_end'     => '</tr>',
                                'heading_cell_start'  => '<th>',
                                'heading_cell_end'    => '</th>',
                                'row_start'           => '<tr>',
                                'row_end'             => '</tr>',
                                'cell_start'          => '<td>',
                                'cell_end'            => '</td>',


                                'table_close'         => '</table>'
                        );

            $this->table->set_template($tmpl); 
            $this->table->set_heading(array('Título', 'Resumo', 'Ações'));
            foreach ($result as $item)
            {
            $nome = $item->nome;
            $detalhes = anchor('painel/empresarial/editar/' . $item->id, 'Editar', 'class="btn btn-warning btn-mini"') . anchor('painel/empresarial/apaga/' . $item->id, 'Remover', array('id' => 'removelink', 'class' => 'btn btn-mini btn-danger')) ;
            $this->table->add_row(array($item->titulo, substr($item->texto, 0, 180), $detalhes));
            }
            echo $this->table->generate();

            ?> 
  <?php endif; ?>
        

</div><!--/span-->