<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_boxes extends MX_Controller
{
    function index(){

            $this->lista();
        }
        /**
         *Lista todos os boxs cadastrados atualmente
         *
         * @return [type] [description]
         */
    function lista()
    {
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',
                    $this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
              {
                    $this->load->library('table');
                    $this->load->model('paginas/box');
                    $data['result'] = $this->box->
                    get_all();

                    $data['title'] = 'Prosperativa - box';
                    $data['module'] = 'boxes';
                    $data['main_content'] = 'e_lista_view';
                    $this->load->view('includes/template', $data);
              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de 
                acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser 
                                                    administrador para realizar essa ação');
              redirect();

              } 
            }
    }
        /**
         * Mostra a página de edição de um box cujo id foi passado como 
         * parâmetro.
         *
         * @param  [int] $id [description]
         * @return [mixed]     [description]
         */
        function editar($id)
        {
            /**
             * Verifica se o usuário está logado para então prosseguir ou não.
             */
            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                //Verifica se o usuário tem nível de acesso permitido
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $id = $this->uri->segment(4);
                    if(!$id)
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('painel/box/lista');
                    }
                    else
                    {
                        $this->load->model('paginas/box');
                        $data['module'] = 'boxes';
                        $data['title'] = 'Prosperativa - boxs - Editar';

                        if($this->box->get_by($id))
                        {
                            $data['integrante'] = $this->box->get_by($id);
                            $data['acao'] = 'editar';
                            $data['main_content'] = 'e_cadastra_view';
                            $this->load->view('includes/template', $data);
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('painel/boxes/lista');
                        }
                    }
                }
                else
                {
                    Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                    $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                    redirect();
                }
            }
        }

        function cadastra()
        {

            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                  if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                  {
                      $data['title'] = 'Prosperativa - boxs - Novo box';
                      $data['module'] = 'box';
                      $data['acao'] = 'cadastra';
                      $data['main_content'] = 'e_cadastra_view';
                      $this->load->view('includes/template', $data);
                  }
                  else
                  {
                  Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                  $this->session->set_flashdata('error', 'Erro de permissão.
                                                        Você precisa ser administrador para realizar essa ação');
                  redirect();

                  }
            }
        }

        function salva(){

            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                $config = array(
                    array(
                        'field' => 'nome',
                        'label' => 'nome',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'texto',
                        'label' => 'texto',
                        'rules' => 'required',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');
                $data['acao'] = $this->input->post('acao');

                if($this->form_validation->run() == FALSE ){
                      $data['title'] = 'Prosperativa - boxs - Novo box';
                      $data['module'] = 'box';
                      $data['acao'] = 'cadastra';
                      $data['main_content'] = 'e_cadastra_view';
                      $this->load->view('includes/template', $data);
                }
                else
                {   //Verifica se foi feito o upload de uma imagem
                    if(strlen($_FILES["imagem"]["name"])>0)
                    {
                        $config['upload_path'] = './assets/img/box/';
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = '4000';
                        $config['max_width']  = '1600';
                        $config['max_height']  = '1200';

                        $this->load->library('upload', $config);

                        if ( ! $this->upload->do_upload('imagem'))
                        {
                                $data['error'] = array('error' => $this->upload->display_errors());

                                $data['title'] = 'Prosperativa - box';
                                $data['module'] = 'box';
                                $data['main_content'] = 'e_cadastra_view';
                                $this->load->view('includes/template', $data);
                        }
                        else
                        {
                            $this->load->library('image_moo');
                            //Is only one file uploaded so it ok to use it with $uploader_response[0].
                            $upload_data = $this->upload->data();
                            $file_uploaded = $upload_data['full_path'];
                            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                            if($this->image_moo->load($file_uploaded)->resize(110,140)->save($new_file,true))
                            {

                                 $this->load->model('boxs/box');

                                //prepara o array com os dados para enviar ao model
                                $dados = array(
                                        'nome' => $this->input->post('nome'),
                                        'texto' => $this->input->post('texto'),
                                        'imagem' => $upload_data['file_name'],
                                    );

                                if( ! $this->box->cadastra_imagem($dados))
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/box/cadastra');
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'Integrante cadastrado
                                    com sucesso!');
                                    redirect('painel/box/lista');
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/box/cadastra');
                            }
                        }
                    }
                    else
                    {
                        $this->load->model('boxs/box');

                        //prepara o array com os dados para enviar ao model
                        $dados = array(
                                'nome' => $this->input->post('nome'),
                                'texto' => $this->input->post('texto'),
                            );

                        if( ! $this->box->cadastra($dados))
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('painel/box/cadastra');
                        }
                        else
                        {
                            $this->session->set_flashdata('success', 'Integrante cadastrado
                            com sucesso!');
                            redirect('painel/box/lista');
                        }
                    }
                }
            }
        }

        function atualiza()
        {
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $config = array(
                        array(
                            'field' => 'texto',
                            'label' => 'texto',
                            'rules' => 'required',
                        ),
                    );
                    $this->load->library('form_validation');
                    $this->form_validation->set_rules($config);
                    $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                    if($this->form_validation->run() == FALSE )
                    {
                        $this->load->model('paginas/box');
                        $id  = $this->input->post('id');
                        $data['integrante'] = $this->box->get_by($id);
                        $data['acao'] = 'editar';
                        $data['main_content'] = 'e_cadastra_view';
                        $this->load->view('includes/template', $data);
                    }
                    else
                    {
                            $this->load->model('paginas/box');

                            //prepara o array com os dados para enviar ao model
                            $dados = array(
                                    'id' => $this->input->post('id'),
                                    'texto' => $this->input->post('texto'),
                                );

                                if( ! $this->box->atualiza($dados))
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/boxes/atualiza/' . $this->input->post('id'));
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'Registro alterado
                                    com sucesso!');
                                    redirect('painel/boxes/lista');
                                }
                    }

                  }
                  else
                  {
                  Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                  $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                  redirect();

                  }
            }
        }

        function apaga($id)
        {
            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Prosperativa - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $id = $this->uri->segment(4);
                    if(!$id)
                    {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                    redirect('painel/box/lista');
                    }
                    else
                    {
                        $this->load->model('boxs/box');
                        if($this->box->delete_box($id))
                        {
                            $this->session->set_flashdata('success', 'Registro apagado
                            com sucesso');
                             redirect('painel/box/lista');
                        }
                         else
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                             realizada, tente novamente ou entre em contado com o suporte');
                            redirect('painel/box/lista');
                        }
                    }
                }
                else
                {
                    Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                    $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                    redirect();

                }
            }

        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */