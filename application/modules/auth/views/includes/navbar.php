    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">Consistere</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li><a href="#">Painel</a></li>
              <li><a href="#about">Comercial</a></li>
              <li><a href="#">Finanças</a>
               <li  class="active"><a href="<?php echo base_url(); ?>index.php/jobs/">Recursos Humanos</a></li>
              <li><a href="#contact">Ajuda</a></li>
            </ul>
            <ul class="nav pull-right">
                <?php if ($this->tank_auth->is_logged_in()): ?>
                  <li id="user-menu" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <?php
                          $username = $this->tank_auth->get_username();
                          echo 'Logado como '.$username;
                      ?>
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><?php echo anchor('logout', ' logout'); ?></li>
                    </ul>
                  </li>
                  <?php endif; ?>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">