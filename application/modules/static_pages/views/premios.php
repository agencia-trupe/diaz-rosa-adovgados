<div class="conteudo conteudo-premios">
	<div class="clearfix"></div>
	<div class="header">
		<h1>Prêmios</h1>
	</div>
	<div class="texto">

		<img src="<?php echo base_url(); ?>assets/img/premios.png" alt="Prêmio os 100 Melhores Fornecedores de RH"
		title="Prêmio os 100 Melhores Fornecedores de RH" class="left">
		
		<div class="descricao right">
			<h3>Melhores Fornecedores para RH</h4>
	 		<p>
	 			Ganhadora do prêmio em 2008, 2009, 2010 e 2011, após pesquisa nacional 
	 			realizada pela Gestão & RH Editora, com os gestores de pessoas das 
	 			maiores empresas brasileiras, a Meta RH figurou entre "Os 100 Melhores 
	 			Fornecedores para RH" e foi a Empresa Destaque na Categoria Talentos 
	 			no Segmento Trabalho Efetivo e Temporário em 2011 e 2012.
	 		</p>
		</div>
		
		<div class="clearfix"></div>
		<div class="separador"></div>

		<img src="<?php echo base_url(); ?>assets/img/topofmind.jpg" alt="Prêmio Top of Mind Estadão de RH" 
		title="Prêmio Top of Mind Estadão de RH" id="topofmind" class="left">

		<div class="descricao right">
			<h3>Top of Mind Estadão de RH</h3> 
			<p>
				A Meta RH foi premiada pela Editora Fênix - Revista Profissional & Negócios
				 nos anos de 2008 e 2010 por ser uma das cinco empresas mais lembradas como
				  fornecedora de mão de obra terceirizada e temporária.
			</p>
		</div>

		<div class="clearfix"></div>
		<div class="separador"></div>

	</div>
	<div class="clearfix"></div>
</div>