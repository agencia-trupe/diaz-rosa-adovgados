<div class="conteudo-empresa">
    <div class="texto left">
       <?=$conteudo->texto; ?>

        <div class="quote">
            <div class="clearfix"></div>
            <p>
                Assim é a <span>Prosperativa</span>: <br>
                <span>ousadia</span> nas idéias, <br>
                <span>ação</span> na implementação e <br>
                <span>prosperidade</span> nos resultados.
            </p>
        </div>
    </div>
    <div class="apresentacao">
        <div class="topo">
            <div class="clearfix"></div>
            <h1>Apresentação Institucional</h1>
            <p>Veja quem somos, no que acreditamos <br>
                e o que fazemos.
            </p>
        </div>
        <a href="<?=site_url('empresa/apresentacao/institucional'); ?>" class="botao">
            <div class="clearfix"></div>
            <span>Clique aqui para baixar!</span>
        </a>
    </div>
</div>