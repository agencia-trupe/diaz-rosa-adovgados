<div class="conteudo home">
   <div class="inner">
   		<div class="slider-home-wrapper">
   			<div class="sliderhome">
   				<?php foreach ($slides as $slide): ?>
   					<a href="<?php echo $slide->link ?>" class="slide">
		   				<div class="barra-lateral">
			   				<div class="barra top"></div>
			   				<div class="barra-vermelha"></div>
			   				<div class="barra bottom"></div>
			   			</div>
			   			<img src="<?php echo base_url('assets/img/slides/' . $slide->imagem) ?>" alt="<?php echo $slide->titulo ?>">
			   			<div class="slider-texto">
							<h1><span><?php echo $slide->titulo ?></span></h1>
							<div class="slider-texto-content">
								<?php echo $slide->texto ?>
							</div>
			   			</div>
		   			</a>
   				<?php endforeach ?>
	   		</div>
   		</div>
   		<div class="clearfix"></div>
   		<div class="barra-texto-home"></div>
   		<div class="texto-home">
   			<span><?php echo $boxes['central']->texto ?></span>
   		</div>
   		<a href="<?php echo site_url('contato') ?>" class="contato-home"><span>Entre em contato</span></a>
    	<div class="perfil-atuacao">
			<div class="quem-somos">
				<h1>Instituições de Ensino</h1>
				<div class="quem-somos-home-conteudo">
					<div class="quem-somos-home-imagem">
						<img src="<?php echo base_url('assets/img/quem-somos-home.jpg') ?>" alt="Diaz Rosa - Quem Somos">
					</div>
					<div class="quem-somos-home-texto">
						<?php echo $boxes['quemsomos']->texto ?>
					</div>
					<a href="<?php echo site_url('setor-educacional') ?>" class="home-saiba-mais">
						<span>saiba mais &raquo;</span>
					</a>
				</div>
			</div>
			<div class="atuacao">
				<h1 class="juridico-home">Departamento Jurídico Externo para sua Empresa</h1>
				<div class="atuacao-home-conteudo">
					<div class="atuacao-home-imagem">
						<img src="<?php echo base_url('assets/img/atuacao-home.jpg') ?>" alt="Diaz Rosa - Quem Somos">
					</div>
					<div class="atuacao-home-texto">
						<?php echo $boxes['atuacao']->texto ?>
					</div>
					<a href="<?php echo site_url('consultoria-juridica-empresarial') ?>" class="home-saiba-mais">
						<span>saiba mais &raquo;</span>
					</a>
				</div>
			</div>
    	</div>
    </div>
</div>