<div class="conteudo-perfil">
	<div class="explicativo">
		<?php echo $texto_perfil->texto; ?>
	</div>
	<div class="cvs">
		<?php foreach($equipe as $socio): ?>
		<div class="cv">
			<?php if($socio->foto != NULL): ?>
				<img src="<?php echo base_url(); ?>assets/img/equipe/<?php echo $socio->foto; ?>" alt="<?=$socio->nome; ?>">
			<?php else: ?>
			<div class="imagem"></div>
			<?php endif; ?>
			<div class="texto">
				<h1><?=$socio->nome; ?></h1>
				<p><?=$socio->texto; ?></p>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php endforeach; ?>
	</div>
</div>