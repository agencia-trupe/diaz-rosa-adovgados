<?php

Class Static_pages extends MX_Controller
{

        public function index()
        {
                $this->home();
        }

        public function home()
        {
                $data = array(
                        'titulo' => 'Home',
                        'page_title' => 'Assim é a <br>Prosperativa',
                        'page_description' => '<span>ousadia</span> nas idéias,<br>
                                               <span>ação</span> na implementação e
                                              <br><span>prosperidade</span> nos
                                               resultados',
                        'pagina' => 'home',
                        'conteudo_principal' => 'static_pages/home'
                );
                $this->load->model('admin_slideshow/slide');
                $data['slides'] = $this->slide->get_all();

                $this->load->model('paginas/box');
                $data['boxes'] = array(
                        'quemsomos' => $this->box->get_box(1),
                        'atuacao'   => $this->box->get_box(2),
                        'central'   => $this->box->get_box(6),
                    );
                $this->load->view('layout/template', $data);
        }

        public function empresa()
        {
             $data = array(
                        'titulo' => 'Empresa',
                        'page_title' => 'A Prosperativa',
                        'page_description' => 'Quem somos e o que fazemos',
                        'pagina' => 'empresa',
                        'conteudo_principal' => 'static_pages/empresa'
                );
             $this->load->model('paginas/pagina');
             $data['conteudo'] = $this->pagina->get_conteudo('empresa');
             $this->load->view('layout/template', $data);
        }

        public function perfil()
        {
            $data = array(
                        'titulo' => 'Perfil',
                        'page_title' => 'Perfil',
                        'page_description' => 'Conheça nossos sócios',
                        'pagina' => 'perfil',
                        'conteudo_principal' => 'static_pages/perfil'
                );
            $this->load->model('equipes/equipe');
            $this->load->model('paginas/box');
            $data['texto_perfil'] = $this->box->get_box('box-perfil');
            $data['equipe'] = $this->equipe->get_all();
            $this->load->view('layout/template', $data);
        }

        public function apresentacao($tipo)
        {
            switch ($tipo) {
                case 'institucional':
                    $file = 'Prosperativa_completo.pdf';
                    break;
                case 'consultoria':
                    $file = 'Prosperativa_Consultoria.pdf';
                    break;
                case 'educacao':
                    $file = 'Prosperativa_EducacaoNegocios.pdf';
                    break;
            }
            $this->load->helper('download');
            $data = file_get_contents( APPPATH . '../assets/files/' . $file);
            $name = $file;
            force_download($name, $data);
        }

        public function servicos()
        {
                $data['titulo'] = 'Prosperativa &middot; Para Empresas';
                $data['pagina'] = 'servicos';
                $data['conteudo_principal'] = "static_pages/servicos";
                $this->load->view('layout/template', $data);
        }

        public function empresas()
        {
                $data['titulo'] = 'Prosperativa &middot; Para Empresas';
                $data['pagina'] = 'empresas';
                $data['conteudo_principal'] = "static_pages/empresas";
                $this->load->view('layout/main', $data);
        }

        public function clientes()
        {       
                $data['titulo'] = 'Prosperativa &middot; Cases | Clientes';
                $data['pagina'] = 'clientes';
                $data['conteudo_principal'] = "static_pages/clientes";
                $this->load->view('layout/main', $data);
        }

        /*public function novidades()
        {       
                $data['titulo'] = 'Prosperativa &middot; Novidades | Artigos';
                $data['pagina'] = 'novidades';
                $data['conteudo_principal'] = "static_pages/novidades";
                $this->load->view('layout/main', $data);
        }*/

}