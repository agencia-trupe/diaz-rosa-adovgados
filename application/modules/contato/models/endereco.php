<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Endereço
*
*
* @author Nilton de Freitas
* @link http://trupe.net
*
*/
Class Endereco extends Datamapper{
    var $table = 'enderecos';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }

    function get_endereco()
    {
        $endereco = new Endereco();
        $endereco->where('id', '1');
        $endereco->limit(1);
        $endereco->get();

        return $endereco;
    }

    function atualiza($dados)
    {
        $endereco = new Endereco();
        $endereco->where('id', 1)->get();
        $update = $endereco->update(array(
                'rua' => $dados['rua'],
                'bairro' => $dados['bairro'],
                'cep' => $dados['cep'],
                'telefone' => $dados['telefone'],
                'telefone2' => $dados['telefone2'],
                'email' =>  $dados['email']
            ));
        return $update;
    }

}