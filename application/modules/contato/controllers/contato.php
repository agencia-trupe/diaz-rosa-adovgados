<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Contato
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class Contato extends MX_Controller
{
    /**
     * Exibe o formulário de contato
     */
    public function index()
    {
        $data = array(
            'titulo' => 'Contato',
            'page_title' => 'Contato',
            'page_description' => 'Uma maneira rápida de solicitar informações
            ou visita na sua empresa',
            'pagina' => 'contato',
            'conteudo_principal' => 'contato/index'
                );
            $this->load->view('layout/template', $data);
    }

    function endereco($tipo = NULL)
    {
        $this->load->model('endereco');
        $data['endereco'] = $this->endereco->get_endereco();
        if($tipo == 'header')
        {
            $this->load->view('contato/endereco_header', $data);
        }
        elseif($tipo == 'contato')
        {
            $this->load->view('contato/endereco_contato', $data);
        }
        elseif($tipo == 'rodape')
        {
            $this->load->view('contato/endereco_rodape', $data);
        }
        elseif($tipo == 'mail')
        {
            $this->load->view('contato/endereco_mail', $data);
        }
    }

    function correspondente()
    {
        $this->load->model('contato/correspondente');
        $data['correspondente'] = $this->correspondente->get_correspondente();
        $this->load->view('contato/correspondente', $data);
    }

    function ajax_check() 
    {
        $this->load->library('form_validation');
        $this->load->library('typography');

        if($this->input->post('ajax') == '1') 
        {
            $this->form_validation->set_error_delimiters('','');
             $this->form_validation->set_rules('nome', 'nome', 'required|xss_clean|max_length[255]');
            $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required|xss_clean');
            $this->form_validation->set_rules('telefone', 'telefone', 'required|xss_clean|max_length[18]');
            $this->form_validation->set_rules('mensagem', 'mensagem', 'required|xss_clean|min_length[10]');
            if($this->form_validation->run() == FALSE) 
            {
                    echo validation_errors();
            } 
            else 
            {
                $this->load->library('MY_PHPMailer');
                $mail = new PHPMailer();
                // Define os dados do servidor e tipo de conexão
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->IsSMTP(); // Define que a mensagem será SMTP
                //$mail->Host = "localhost"; // Endereço do servidor SMTP (caso queira utilizar a autenticação, utilize o host smtp.seudomínio.com.br)
                $mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
                $mail->Username = 'nilton@trupe.net'; // Usuário do servidor SMTP (endereço de email)
                $mail->Password = '6feynman'; // Senha do servidor SMTP (senha do email usado)
                
                $mail->Host = 'smtp.trupe.net';
                $mail->Port = '587'; 
                // Define o remetente
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->From = "nilton@trupe.net"; // Seu e-mail
                $mail->Sender = "nilton@trupe.net"; // Seu e-mail
                $mail->FromName = $this->input->post('nome'); // Seu nome
                 
                // Define os destinatário(s)
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->AddAddress('niltondefreitas@gmail.com', 'Nilton');
                //$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
                //$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta
                 
                // Define os dados técnicos da Mensagem
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
                //$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)
                 
                // Define a mensagem (Texto e Assunto)
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->Subject  = "Contato do Site Stato Móveis"; // Assunto da mensagem
                
                $data['dados'] = array(
                                    'nome' => $this->input->post('nome'),
                                    'email' => $this->input->post('email'),
                                    'telefone' => $this->input->post('telefone'),
                                    'mensagem' => $this->typography->auto_typography($this->input->post('mensagem')),
                            );
                $email_view = $this->load->view('contato/email', $data, TRUE);

                $mail->Body = $email_view;


                if(!$mail->Send()) {
                  echo "Erro: " . $mail->ErrorInfo;
                } else {
                  echo "Mensagem enviada com sucesso!";
                }
            }
        }
    }
}

