<div class="span9">

<legend><?php echo $acao == 'editar' ? 'Editar evento' : 'Novo evento' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php
    echo form_open_multipart(($acao == 'editar') ? 'painel/eventos/atualiza' : 'painel/eventos/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php if($acao == 'edita')
      {
        echo form_hidden('acao', 'edita');
      }
      else{
        echo form_hidden('acao', 'cadastra');
      }
      ?>  
      </div>
    <?php if($acao == 'editar') : ?>
    <?php echo
    form_hidden('id', set_value('id', $evento->id));
    ?>
  <?php endif; ?>
    <div class="row-fluid">
        <div class="span9">
            <label for="">Título</label>
            <?php echo form_input(array('name'=>'titulo', 'id' => 'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $evento->titulo : ''), 'class' => 'span12',)); ?>
            <?php echo form_error('titulo'); ?>
        </div>
    </div>
  <div class="row-fluid">
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="conteudo">Conteúdo</label>
            <div class="controls">
              <?php echo form_textarea('conteudo', set_value('conteudo', $acao == 'editar' ? $evento->texto : ''), 'class="tinymce span11"'); ?>
              <span class="help-inline"><?php echo form_error('conteudo'); ?></span>
            </div>
      </div>
      <div class="control-group">
            <label class="control-label" for="data">Data</label>
            <div class="controls">
              <?php echo form_input('data', set_value('data', $acao == 'editar' ? date('d/m/Y', $evento->data) : ''), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('data'); ?></span>
            </div>
      </div>
  </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>