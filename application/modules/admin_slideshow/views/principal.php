<div class="span12">
    <?php if(isset($error)){var_dump($error);} ?>
    <legend>Alterar foto principal - Unidade <?=$unidade->nome; ?></legend>
    <h5>Foto Atual</h5>
    <img src="<?=base_url(); ?>assets/img/unidades/<?=$unidade->foto_principal; ?>" alt="">
    <br>
    <h5>Nova Foto</h5>
    <?=form_open_multipart('painel/fotos/upload'); ?>
    <?=form_hidden('unidade_id', $unidade->id); ?>
    <?=form_upload('foto_principal'); ?>
    <div class="clearfix"></div>
    <br>
    <?=form_submit('salvar', 'Salvar', 'class="btn btn-success"'); ?>
</div>