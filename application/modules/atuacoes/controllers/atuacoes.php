<?php

Class Atuacoes extends MX_Controller
{
    public function index()
    {
        $data = array(
                    'titulo' => 'Áreas de Atuação',
                    'page_title' => 'Setor Educacional',
                    'page_description' => '',
                    'pagina' => 'atuacao',
                    'conteudo_principal' => 'atuacoes/index'
            );
        $data['result_count'] = $this->db->get('atuacoes')->num_rows();
        $this->load->model('atuacoes/atuacao');
        $data['atuacoes'] = $this->atuacao->get_all();
        $this->load->view('layout/template', $data);
    }
}