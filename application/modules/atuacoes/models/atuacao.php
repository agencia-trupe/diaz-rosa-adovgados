<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Pagna
*
*
* @author Nilton de Freitas
* @link http://trupe.net
*
*/
Class Atuacao extends Datamapper{
    var $table = 'atuacoes';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }

    function get_resumo($slug)
    {
        $pagina = new Pagina();
        $pagina->where('slug', $slug);
        $pagina->limit(1);
        $pagina->get();

        $resumo = substr(strip_tags($pagina->texto), 0, 160);

        return $resumo;
    }

    function get_conteudo($id)
    {
        $pagina = new Atuacao();
        $pagina->where('id', $id);
        $pagina->limit(1);
        $pagina->get();

        return $pagina;
    }


    function get_all()
    {
        $programa = new Atuacao();
        $programa->get();

        $arr = array();
        foreach($programa->all as $programas)
        {
            $arr[] = $programas;
        }

        return $arr;

    }


    function atualiza($dados){

        $pagina = new Atuacao();
        $pagina->where('id', $dados['id']);
        $update = $pagina->update(array(
            'titulo' => $dados['titulo'],
            'texto' => $dados['texto'],
        ));
        return $update;
    }

    function cadastra($dados){

        $edu = new Atuacao();
        $edu->titulo = $dados['titulo'];
        $edu->texto = $dados['texto'];
        if( ! $edu->save() ) return FALSE;
        return TRUE;
    }

    function get_programa_by_id($id)
    {
        $programa = new Programa();
        $programa->where('id', $id);
        $programa->limit(1);
        $programa->get();

        return $programa;
    }

     function delete_atuacao($id){
        $edu = new Atuacao();
        $edu->where('id', $id)->get();
        

        if($edu->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    
}