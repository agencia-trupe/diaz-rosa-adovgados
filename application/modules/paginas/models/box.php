<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Box
*
* @package Prosperativa
* @author Nilton de Freitas
* @link http://trupe.net
*
*/
Class Box extends Datamapper{
    var $table = 'boxes';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna o conteúdo de um box da home page.
     * @param  string $titulo título do box
     * @return array
     */
    function get_box($id)
    {
        $box = new Box();
        $box->where('id', $id);
        $box->limit(1);
        $box->get();

        return $box;
    }

    function get_by($id)
    {
        $box = new Box();
        $box->where('id', $id);
        $box->limit(1);
        $box->get();

        return $box;
    }

    function get_all()
    {
        $i = new Box();
        $i->order_by('titulo', 'asc');
        $i->get();

        $arr = array();
        foreach($i->all as $integrante)
        {
            $arr[] = $integrante;
        }

        return $arr;

    }

    function atualiza($dados)
    {
        $equipe = new Box();
        $equipe->where('id', $dados['id']);
        $update = $equipe->update(array(
            'texto' => $dados['texto'],
        ));
        return $update;
    }
}