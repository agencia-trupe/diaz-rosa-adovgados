<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Pagna
*
*
* @author Nilton de Freitas
* @link http://trupe.net
*
*/
Class Pagina extends Datamapper{
    var $table = 'paginas';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }

    function get_resumo($slug)
    {
        $pagina = new Pagina();
        $pagina->where('slug', $slug);
        $pagina->limit(1);
        $pagina->get();

        $resumo = substr(strip_tags($pagina->texto), 0, 160);

        return $resumo;
    }

    function get_conteudo($slug)
    {
        $pagina = new Pagina();
        $pagina->where('slug', $slug);
        $pagina->limit(1);
        $pagina->get();

        return $pagina;
    }
    /**
     * Retorna os programas ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de programas ativos
     */
    function get_all($tipo, $limit, $offset)
    {
        $programa = new Programa();
        $programa->where('programa_categoria', $tipo);
        $programa->order_by('programa_data_expiracao', 'asc');
        $programa->get($limit, $offset);

        $arr = array();
        foreach($programa->all as $programas)
        {
            $arr[] = $programas;
        }

        return $arr;

    } 

    function get_home()
    {
        $programa = new Programa();
        $programa->order_by('programa_data_expiracao', 'asc');
        $programa->get(2);

        $arr = array();
        foreach($programa->all as $programas)
        {
            $arr[] = $programas;
        }

        return $arr;

    }

    function get_all_all($limit, $offset)
    {
        $programa = new Programa();
        $programa->order_by('programa_data_expiracao', 'asc');
        $programa->order_by('programa_data_cadastro', 'desc');
        $programa->get($limit, $offset);

        $arr = array();
        foreach($programa->all as $programas)
        {
            $arr[] = $programas;
        }

        return $arr;

    }



    function cadastra($dados){

        $programa = new programa();

        $programa->programa_categoria = $dados['categoria'];
        $programa->programa_empresa = $dados['empresa'];
        $programa->programa_link = $dados['link'];
        $programa->programa_imagem = $dados['imagem'];
        $programa->programa_data_expiracao = strtotime(str_replace('/', '-', $dados['data_expiracao']));;
        $programa->programa_destaque = $dados['destaque'];
        $programa->programa_data_cadastro = time();

        if($programa->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    function atualiza_imagem($dados){

        $programa = new programa();
        $programa->where('id', $dados['id']);
        $update = $programa->update(array(
            'programa_categoria' => $dados['categoria'],
            'programa_empresa' => $dados['empresa'],
            'programa_link' => $dados['link'],
            'programa_imagem' => $dados['imagem'],
            'programa_data_expiracao' => strtotime(str_replace('/', '-',
                $dados['data_expiracao'])),
            'programa_destaque' => $dados['destaque'],
        ));
        return $update;
    }

    function atualiza($dados){

        $pagina = new Pagina();
        $pagina->where('slug', $dados['slug']);
        $update = $pagina->update(array(
            'texto' => $dados['texto'],
        ));
        return $update;
    }
    function get_programa_by_id($id)
    {
        $programa = new Programa();
        $programa->where('id', $id);
        $programa->limit(1);
        $programa->get();

        return $programa;
    }

     function delete_programa($id){
        $programa = new Programa();
        $programa->where('id', $id)->get();
        

        if($programa->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    
}