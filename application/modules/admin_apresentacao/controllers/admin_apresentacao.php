<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_apresentacao extends MX_Controller
{
    function index(){

        $this->editar();
    }
    function editar()
    {
        /**
         * Verifica se o usuário está logado para então prosseguir ou não.
         */
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Prosperativa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
            
        {
            //Verifica se o usuário tem nível de acesso permitido
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                
                $this->load->model('empresas/empresa');
                $data['module'] = 'empresa';
                $data['title'] = 'Prosperativa - Apresentação - Editar';

                $data['empresa'] = $this->empresa->get_conteudo('1');
                $data['main_content'] = 'a_edita_view';
                $this->load->view('includes/template', $data);
                
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();
            }
        }
    }

    function atualiza()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Prosperativa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $config = array(
                    array(
                        'field' => 'texto',
                        'label' => 'texto',
                        'rules' => 'required',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                if($this->form_validation->run() == FALSE )
                {
                    $this->load->model('empresas/empresa');
                    $data['module'] = 'empresa';
                    $data['title'] = 'Prosperativa - Apresentação - Editar';
                    $data['empresa'] = $this->empresa->get_conteudo(1);
                    $data['main_content'] = 'a_edita_view';
                    $this->load->view('includes/template', $data);
                }
                else
                {
                    

                    $this->load->model('empresas/empresa');

                    //prepara o array com os dados para enviar ao model
                    $dados = array(
                            'texto' => $this->input->post('texto'),
                        );


                        if( ! $this->empresa->atualiza($dados))
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('painel/empresa/');
                        }
                        else
                        {
                            $this->session->set_flashdata('success', 'empresa atualizado
                            com sucesso!');
                            redirect('painel/empresa');
                        }

                }

              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */