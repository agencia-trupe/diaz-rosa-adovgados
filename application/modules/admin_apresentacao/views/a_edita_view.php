<div class="span12">
  <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
  <legend>Empresa</legend>
    <?php
    echo form_open_multipart('painel/empresa/atualiza', 'class="well"'); 
    ?>
  <div class="row-fluid">
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="texto">Conteúdo</label>
            <div class="controls">
              <?php echo form_textarea('texto', set_value('texto', $empresa->texto), 'class="tinymce span11"'); ?>
              <span class="help-inline"><?php echo form_error('texto'); ?></span>
            </div>
      </div>
  <?php echo form_submit('submit', 'Salvar', 'class="btn btn-primary"'); ?>
  <?php echo form_close();   ?> 

</div>
</div>