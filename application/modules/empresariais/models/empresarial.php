<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Pagna
*
*
* @author Nilton de Freitas
* @link http://trupe.net
*
*/
Class Empresarial extends Datamapper{
    var $table = 'empresariais';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }

    function get_resumo($slug)
    {
        $pagina = new Pagina();
        $pagina->where('slug', $slug);
        $pagina->limit(1);
        $pagina->get();

        $resumo = substr(strip_tags($pagina->texto), 0, 160);

        return $resumo;
    }

    function get_conteudo($id)
    {
        $pagina = new Empresarial();
        $pagina->where('id', $id);
        $pagina->limit(1);
        $pagina->get();

        return $pagina;
    }


    function get_all()
    {
        $programa = new Empresarial();
        $programa->get();

        $arr = array();
        foreach($programa->all as $programas)
        {
            $arr[] = $programas;
        }

        return $arr;

    }


    function atualiza($dados){

        $pagina = new Empresarial();
        $pagina->where('id', $dados['id']);
        $update = $pagina->update(array(
            'titulo' => $dados['titulo'],
            'texto' => $dados['texto'],
        ));
        return $update;
    }

    function cadastra($dados){

        $edu = new Empresarial();
        $edu->titulo = $dados['titulo'];
        $edu->texto = $dados['texto'];
        if( ! $edu->save() ) return FALSE;
        return TRUE;
    }

    function get_programa_by_id($id)
    {
        $programa = new Programa();
        $programa->where('id', $id);
        $programa->limit(1);
        $programa->get();

        return $programa;
    }

     function delete_educacional($id){
        $edu = new Empresarial();
        $edu->where('id', $id)->get();
        

        if($edu->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    
}