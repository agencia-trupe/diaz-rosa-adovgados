<div class="conteudo empresarial">
    <h1 class="titulo-pagina">Consultoria Jurídica Empresarial</h1>
    <div class="conteudo-pagina">
        <div class="pagina-sidebar">
            <div class="pagina-nav-wrapper">
                        <?php for ($i=0; $i < $result_count; $i++): ?>
                            <a  data-slide-index="<?php echo $i ?>" href="#"><?php echo $empresariais[$i]->titulo ?></a></li>
                        <?php endfor ?>
                </nav>
            </div>
            <div class="clearfix"></div>
            <div class="pagina-imagem-wrapper">
                <img src="<?php echo base_url('assets/img/consultoria-empresarial.jpg') ?>" alt="Diaz Rosa - Setor Educacional">
            </div>
        </div>
   		<div class="pagina-texto">
               <ul class="pagina-slider">
                  <?php foreach ($empresariais as $empresarial): ?>
                     <li class="texto-slide">
                        <h2><?php echo $empresarial->titulo ?></h2>
                        <?php echo $empresarial->texto ?>
                     </li>
                  <?php endforeach ?>
               </ul>
   		</div>
   </div>
   <div class="clearfix"></div>
</div>