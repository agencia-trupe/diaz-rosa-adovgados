<?php

Class Empresariais extends MX_Controller
{

        public function index()
        {
             $data = array(
                        'titulo' => 'Consultoria Jurídica Empresarial',
                        'page_title' => 'Setor Educacional',
                        'page_description' => 'Consultoria Jurídica Empresarial',
                        'pagina' => 'consultoria-juridica',
                        'conteudo_principal' => 'empresariais/index'
                );
             $data['result_count'] = $this->db->get('empresariais')->num_rows();
             $this->load->model('empresariais/empresarial');
             $data['empresariais'] = $this->empresarial->get_all();
             $this->load->view('layout/template', $data);
        }

}