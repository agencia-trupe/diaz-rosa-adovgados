<div class="span12">
  <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
  <legend>Contato</legend>
    <?php
    echo form_open_multipart('painel/contato/atualiza', 'class="well"'); 
    ?>
  <div class="row-fluid">
      <div class="clearfix"></div>
      <!--Endereço-->
      <div class="control-group">
            <label class="control-label" for="rua">Endereço</label>
            <div class="controls">
              <?php echo form_input('rua', set_value('rua', $contato->rua), 'class="span16"'); ?>
              <span class="help-inline"><?php echo form_error('rua'); ?></span>
            </div>
      </div>
      <!--fim de Endereço-->
      <!--Bairro-->
      <div class="control-group">
            <label class="control-label" for="bairro">Bairro</label>
            <div class="controls">
              <?php echo form_input('bairro', set_value('bairro', $contato->bairro), 'class="span16"'); ?>
              <span class="help-inline"><?php echo form_error('bairro'); ?></span>
            </div>
      </div>
      <!--fim de Bairro-->
      <!--CEP-->
      <div class="control-group">
            <label class="control-label" for="cep">CEP</label>
            <div class="controls">
              <?php echo form_input('cep', set_value('cep', $contato->cep), 'class="span16"'); ?>
              <span class="help-inline"><?php echo form_error('cep'); ?></span>
            </div>
      </div>
      <!--fim de CEP-->
      <!--Telefone-->
      <div class="control-group">
            <label class="control-label" for="telefone">Telefone</label>
            <div class="controls">
              <?php echo form_input('telefone', set_value('telefone', $contato->telefone), 'class="span16"'); ?>
              <span class="help-inline"><?php echo form_error('telefone'); ?></span>
            </div>
      </div>
      <!--fim de Telefone-->
      <!--Telefone 2-->
      <div class="control-group">
            <label class="control-label" for="telefone2">Telefone 2</label>
            <div class="controls">
              <?php echo form_input('telefone2', set_value('telefone2', $contato->telefone2), 'class="span16"'); ?>
              <span class="help-inline"><?php echo form_error('telefone2'); ?></span>
            </div>
      </div>
      <!--fim de Telefone 2-->
      <!--Email-->
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="email">Email</label>
            <div class="controls">
              <?php echo form_input('email', set_value('email', $contato->email), 'class="span16"'); ?>
              <span class="help-inline"><?php echo form_error('email'); ?></span>
            </div>
      </div>
      <!--fim de Email-->
  <?php echo form_submit('submit', 'Salvar', 'class="btn btn-primary"'); ?>
  <?php echo form_close();   ?> 

</div>
</div>