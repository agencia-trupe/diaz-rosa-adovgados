<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_contato extends MX_Controller
{
    function index(){

        $this->editar();
    }
    function editar()
    {
        /**
         * Verifica se o usuário está logado para então prosseguir ou não.
         */
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Prosperativa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            //Verifica se o usuário tem nível de acesso permitido
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                
                $this->load->model('contato/endereco');
                $data['module'] = 'contato';
                $data['title'] = 'Prosperativa - Contato - Editar';

                if($this->endereco->get_endereco())
                {
                    $data['contato'] = $this->endereco->get_endereco();
                    $data['main_content'] = 'a_edita_view';
                    $this->load->view('includes/template', $data);
                }
                else
                {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                    redirect('empresas/lista');
                }
                
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();
            }
        }
    }

    function atualiza()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $data['main_content'] = 'system/mustLogin';
            $data['title'] = 'Prosperativa - Erro de acesso';
            $this->load->view('start/templatenonav', $data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
            {
                $this->load->model('contato/endereco');

                $config = array(
                    array(
                        'field' => 'telefone',
                        'label' => 'telefone',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'email',
                        'label' => 'email',
                        'rules' => 'required|valid_email',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                if($this->form_validation->run() == FALSE )
                {
                     $data['contato'] = $this->endereco->get_endereco();
                    $data['main_content'] = 'a_edita_view';
                    $data['module'] = 'contato';
                    $this->load->view('includes/template', $data);
                }
                else
                {
                    
                    $dados = array(
                            'rua' => $this->input->post('rua'),
                            'bairro' => $this->input->post('bairro'),
                            'cep' => $this->input->post('cep'),
                            'telefone' => $this->input->post('telefone'),
                            'telefone2' => $this->input->post('telefone2'),
                            'email' => $this->input->post('email'),
                        );

                        if( ! $this->endereco->atualiza($dados))
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('painel/contato/');
                        }
                        else
                        {
                            $this->session->set_flashdata('success', 'empresa atualizado
                            com sucesso!');
                            redirect('painel/contato');
                        }

                }

              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */