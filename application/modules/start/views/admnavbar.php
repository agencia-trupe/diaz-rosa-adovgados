    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo base_url(); ?>">Prosperativa</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="<?php echo ($module == 'boxes') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/boxes">Boxes (home)</a>
              </li>
              <li class="<?php echo ($module == 'empresa') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/apresentacao">Quem Somos</a>
              </li>
              <li class="<?php echo ($module == 'educacional') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/educacional">Setor Educacional</a>
              </li>
              <li class="<?php echo ($module == 'empresarial') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/empresarial">Consultoria Jurídica Empresarial</a>
              </li>
              <li class="<?php echo ($module == 'atuacao') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/atuacao">Áreas de Atuação</a>
              </li>
              <li class="<?php echo ($module == 'contato') ? 'active' : ''; ?>">
                <a href="<?php echo base_url(); ?>painel/contato">Contato</a>
              </li>
            </ul>
            <ul class="nav pull-right">
                <li><?php echo anchor('logout', 'Sair'); ?></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">