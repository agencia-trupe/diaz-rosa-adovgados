<div class="conteudo-perfil">
	<div class="interna">
		<article>
			<h1>A Prosperativa</h1>
			<p>É uma empresa especializada em Programas de Trainees, Estagiários e Menores 
				Aprendizes, denominados Jovens Talentos. Selecionamos jovens com potencial,
				 oferecendo oportunidades de desenvolvimento e crescimento em empresas renomadas, 
				 que os preparam para assumir no futuro, posições desafiadoras.
			</p>
			<p>
				Para cada programa, disponibilizamos uma equipe de profissionais altamente 
				qualificados para efetuar toda a estruturação do processo: Alinhamento de 
				expectativas com o Gestor (Mentor) na empresa, divulgação, inscrição, provas online, 
				dinâmicas, entrevistas e fechamento dos programas. Em todas as etapas temos o 
				acompanhamento da empresa cliente e dos candidatos.			
			</p>
			<p>
				A estruturação dos programas e a elaboração das atividades são feitas de forma 
				criativa e de acordo com o público jovem. Disponibilizamos um espaço moderno e 
				ferramentas de alta qualidade. Os candidatos serão recebidos com todo conforto e 
				atenção que precisam para o desenvolvimento das atividades de seleção.
			</p>
		</article>

		<img width="193" height="499" src="<?php echo base_url(); ?>assets/img/candidato.png" alt="Candidato">

		<div class="clearfix"></div>
		<aside>
			<h1>Diferencial</h1>
			<p>
				Antes de iniciar os processos, estimulamos os jovens talentos a pensarem sobre
				 seus valores relacionados aos valores corporativos e éticos que estarão ajustados 
				 ao seu engajamento na organização. Assim aplicamos o “Inventário C.H.A.V.E.”, 
				 ferramenta que possibilita investigar pontos de aderência dos valores dos candidatos 
				 aos valores organizacionais.
			</p>
		</aside>

		<?php echo Modules::run('clipping'); ?>
		<div class="clearfix"></div>
	</div>
</div>