<div class="conteudo-empresas">
	<div class="interna">
		<h1>Para empresas</h1>
		<div id="accordion">  
		  <h3 id="aprendiz"><a href="#">Jovem aprendiz <div class="seta"></div></a></h3>  
		  <div>
		  	<p>Ser um Jovem Aprendiz é desenvolver-se constantemente.  O programa Jovem 
		  		Aprendiz ajuda a inserir o jovem no mundo do trabalho, apoiado na lei
		  		 10.097/2000, a Lei da Aprendizagem. O Programa Jovem Aprendiz desenvolvido 
		  		 pela Prosperativa contribui para a identificação de jovens preparados 
		  		 para a primeira experiência profissional, proporcionando um ambiente de 
		  		 avaliação prática e desafios crescentes para que, ao final do programa, 
		  		 estejam aptos a ingressar no mercado de trabalho. Destacamos que a 
		  		 contratação do Aprendiz não é feita em regime de estágio, e sim em 
		  		 regime CLT, com um contrato especial por tempo determinado e 
		  		 registro em carteira profissional.  
		  	</p>

		  	<p>
		  		<b>Metodologia</b><br>
					Alinhamento de Expectativas > Triagem de currículos > Testes escritos > 
					Entrevistas coletivas Consultoria > Entrevistas com o supervisor da vaga >
					 Processo Admissional
				</p>
		  </div>
		  <h3 id="estagio"><a href="#">Estágio <div class="seta"></div></a></h3>  
		  <div>
		  	<p>
		  		O principal objetivo do programa de Estágio é preparar os estagiários 
		  		para o mercado de trabalho, propiciando o seu desenvolvimento pessoal 
		  		e profissional por meio da complementação do ensino em termos de treinamento 
		  		prático, aperfeiçoamento técnico, cultural, científico e de relacionamento humano. 
		  		O Programa tem duração de 1 a 2 anos e é destinado a estudantes de curso superior, 
		  		que estejam cursando do segundo ao penúltimo ano do curso possibilitando um 
		  		exercício prático da teoria aprendida em sala de aula.
		  	</p>
		  	<p>
		  		<b>Metodologia</b><br>
		  		Alinhamento de Expectativas > Triagem de currículos > Testes Online >
		  		 Inventário C.H.A.V.E. > Dinâmicas de Grupo > Entrevistas com o Gestor da vaga >
		  		  Processo Admissional > Mentoring
		  	</p>    
		  </div>
		  <h3 id="trainee"><a href="#">Trainee <div class="seta"></div></a></h3>  
		  <div>
		  	<p>
		  		O Programa de Trainee visa selecionar, aprimorar, desenvolver e reter indivíduos com grande potencial, em inicio de carreira, selecionados com o objetivo de prepará-los para assumir posições estratégicas dentro das organizações.
				</p>
				<p>	
					Para o público formado por jovens profissionais é uma grande oportunidade de obter experiência, desenvolver e crescer profissionalmente.
				</p>
				<p>
					Para as empresas o Programa de Trainee cria uma cultura de inovação aliado à formação de futuros profissionais de alto desempenho.
				</p>
				<p>
					<b>Metodologia</b><br>
					Alinhamento de Expectativas > Triagem de currículos > Testes Online > Inventário C.H.A.V.E. > 
					Dinâmicas de Grupo > Painel > Entrevista individual com gestores > Processo Admissional> Mentoring.
				</p>
				<br>
				<p>
					<b>Soluções compartilhadas, nosso diferencial:</b><br>
					A Prosperativa pode ser contratada para programas completos ou por módulos específicos 
					(Taylor Made). Dentro do escopo e necessidade da Empresa – Cliente.
				</p>
				<p>
					A utilização de um sistema capaz de gerenciar todo o processo faz parte de nosso diferencial, 
					assim como, a elaboração de um “Book” que é preparado com todas as informações detalhadas 
					sobre todo o tipo de programa escolhido.
				</p>
				<p>
					A preparação de todas as etapas, principalmente, as dinâmicas de grupo compostas por
					 cases ou games de empresas, são estruturadas e compartilhadas em parceria com o RH da 
					 Empresa – Cliente, para que possa ser representativo da realidade e expectativas
					  relacionadas ao processo de avaliação de cada candidato.
				</p>
				<p> 
					A escolha de ferramentas de avaliação, tais como testes e inventários comportamentais, 
					estão baseados em metodologias testadas e certificadas.
				</p>
		  </div>
		  <h3 id="jovens-talentos"><a href="#">Programa - Jovens Talentos (Trainees e Estagiários)<div class="seta"></div></a></h3>  
		  <div>
		  	<p>
		  		<b>A premissa do Programa</b><br>
		  		Ampliar possibilidades de carreira a jovens que ingressam no mercado de trabalho, 
		  		bem como reter e desenvolver talentos e futuros líderes.
		  	</p>
				<p>
					<b>Nosso objetivo:</b><br>
					<ul>
						<li>&raquo; “Oxigenar” a Cultura Organizacional;</li>
						<li>&raquo; Fomentar o desenvolvimento de novos valores;</li>
						<li>&raquo; Desenvolver novos talentos;</li>
						<li>&raquo; Valorização da Imagem Organizacional;</li>
					</ul>
				</p>
				<p><b>Equipe dedicada</b><br>
					Disponibilizamos um quadro de consultores especializados para pronto atendimento exclusivo, 
					com foco nos negócios de nossos clientes.
				</p>
				<p>
					O atendimento será alinhado com o cliente e a estrutura para atendimento poderá ser ampliada
					 ou adaptada, conforme as necessidades de cada Programa a ser atendido.
				</p>
				<p>
					<b>Mídias Sociais</b><br>
						Utilizamos todos os canais de comunicação com os Jovens Talentos, como notícias, 
						comentários, divulgação de oportunidades e releases nas mídias sociais.
				</p>
				<p>
					<b>Avaliação</b><br>
					Técnicas de avaliação dinâmicas, tais como: Games online e ou presenciais, 
					testes exclusivos desenvolvidos de acordo com o perfil de cada cliente.
				</p>
		  </div>
		  <h3 id="mentoring"><a href="#">Mentoring <div class="seta"></div></a></h3>  
		  <div>
		  	<p>
		  		É um processo de Tutoria, no qual o “Mentor“ (profissional mais experiente) é escolhido 
		  		e preparado para orientar um profissional em início de carreira (Trainee ou Gestor Junior). 
				</p>	
				<p>
					Em alguns casos, pode ser uma assessoria direcionada aos Profissionais mais Seniores que 
					vão ocupar funções de alto escopo estratégico e abrangência (local ou global). 
				</p>
				<p>
					O objetivo é utilizar a vivência e a experiência de um Gestor ou Diretor para preparar 
					o Gestor menos experiente a estar apto e mais preparado para enfrentar situações adversas
					 e estratégicas ao longo de sua Gestão (ex: ampliar a visão sobre Gestão de: processos, 
					 pessoas, recursos, uso de tecnologias e finanças). 
				</p>
				<p>
					<b>Nosso diferencial:</b><br>
					 Preparar, previamente, os Mentores para que possam estar aptos a cumprir
					 o roteiro estratégico e as práticas dentro de seu papel. Aproveitando os recursos já 
					 existentes em cada Mentor.    
				</p>
				<p>
					Assessorar a empresa-cliente na logística, cronograma e no acompanhamento ao processo. 
				</p>
				<p>
					A cada passo neste processo, aplicamos uma avaliação, para verificar a eficácia na 
					absorção e aplicação das informações por parte deste jovem profissional. 
				</p>
				<p>
					Envio de relatório final para o RH da empresa, com conclusões e indicações de próximos
					 passos sugeridos para a continuidade do desenvolvimento do jovem profissional.   
		  	</p>
		  </div>
		</div>
		<?php echo Modules::run('clipping'); ?>
		<div class="clearfix"></div>
	</div>
</div>