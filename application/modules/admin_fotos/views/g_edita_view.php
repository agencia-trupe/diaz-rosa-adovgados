<div class="span12">
<?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?>
<legend>Editar informações</legend>
    <img src="<?php echo base_url(); ?>assets/img/fotos/display/<?php echo $foto->imagem; ?>"
    width="50%" height="50%" alt="<?php echo $foto->titulo; ?>">
    <?php echo form_open('admin_fotos/atualiza'); ?>
      <?php echo form_hidden('id', $foto->id); ?>
      <?php echo form_hidden('tipo', $foto->categoria); ?>
      <div class="clearfix"></div>
      <br>
      <?php echo form_label('Título'); ?>
      <?php echo form_input('titulo', set_value('titulo', $foto->titulo), 'class="span7"'); ?>
      <?php echo form_label('Descrição'); ?>
      <?php echo form_textarea('descricao', set_value('descricao', $foto->descricao), 'class="tinymce5 span7"'); ?>
      <p><b>Caracteres: </b><div id="characterCounter5"></div></p>
      <div class="clearfix"></div>
      <input type="submit" name="test" value="Salvar" />
    </form>
</div>