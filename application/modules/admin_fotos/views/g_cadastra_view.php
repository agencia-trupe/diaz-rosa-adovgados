<div class="span12">
<?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?>
    
      <legend>Adicionar Imagens</legend>
      <?php echo form_open_multipart('admin_fotos/upload/'); ?>
      <div class="well">
        <legend>Arquivo 1</legend>

        <label for="file[]"><b>Arquivo</b></label>
        <input type="file" name="file[]" size="20" />
        <div class="clearfix"></div>
        <?php echo form_label('<b>Título</b>'); ?>
        <?php echo form_input('titulo[]', set_value('titulo[]') , 'class="span7"'); ?>
        <?php echo form_label('<b>Descrição</b>'); ?>
        <?php echo form_textarea('descricao[]', set_value('descricao[]'), 'class="tinymce1 span7"'); ?>
        <br>
        <p><b>Caracteres: </b></p><div id="characterCounter1"></div>
        <div class="clearfix"></div>
      </div>

      <div class="well">
        <legend>Arquivo 2</legend>

        <label for="file[]"><b>Arquivo</b></label>
        <input type="file" name="file[]" size="20" />
        <div class="clearfix"></div>
         <?php echo form_label('<b>Título</b>'); ?>
        <?php echo form_input('titulo[]', set_value('titulo[]') , 'class="span7"'); ?>
        <?php echo form_label('<b>Descrição</b>'); ?>
        <?php echo form_textarea('descricao[]', set_value('descricao[]'), 'class="tinymce2 span7"'); ?>
        <br>
        <p><b>Caracteres: </b></p><div id="characterCounter2"></div>
        <div class="clearfix"></div>
      </div>



     <div class="well">
      <legend>Arquivo 3</legend>

      <label for="file[]"><b>Arquivo</b></label>
      <input type="file" name="file[]" size="20" />
      <div class="clearfix"></div>
       <?php echo form_label('<b>Título</b>'); ?>
        <?php echo form_input('titulo[]', set_value('titulo[]'), 'class="span7"'); ?>
      <?php echo form_label('<b>Descrição</b>'); ?>
      <?php echo form_textarea('descricao[]', set_value('descricao[]'), 'class="tinymce3 span7"'); ?>
      <br>
        <p><b>Caracteres: </b></p><div id="characterCounter3"></div>
        <div class="clearfix"></div>
      </div>

      <div class="well">
      <legend>Arquivo 4</legend>

      <label for="file[]"><b>Arquivo</b></label>
      <input type="file" name="file[]" size="20" />
      <div class="clearfix"></div>
       <?php echo form_label('<b>Título</b>'); ?>
        <?php echo form_input('titulo[]', set_value('titulo[]'), 'class="span7"'); ?>
      <?php echo form_label('<b>Descrição</b>'); ?>
      <?php echo form_textarea('descricao[]', set_value('descricao[]'), 'class="tinymce4 span7"'); ?>
       <br>
        <p><b>Caracteres: </b></p><div id="characterCounter4"></div>
        <div class="clearfix"></div>
      </div>
      <input type="submit" name="test" value="Salvar" />
    </form>
</div>