<div class="conteudo educacional">
    <h1 class="titulo-pagina">Setor Educacional</h1>
    <div class="conteudo-pagina">
        <div class="pagina-sidebar">
            <div class="pagina-nav-wrapper">
                        <?php for ($i=0; $i < $result_count; $i++): ?>
                            <a  data-slide-index="<?php echo $i ?>" href="#"><?php echo $educacionais[$i]->titulo ?></a></li>
                        <?php endfor ?>
                </nav>
            </div>
            <div class="clearfix"></div>
            <div class="pagina-imagem-wrapper">
                <img src="<?php echo base_url('assets/img/setor-educacional.jpg') ?>" alt="Diaz Rosa - Setor Educacional">
            </div>
        </div>
   		<div class="pagina-texto">
               <ul class="pagina-slider">
                  <?php foreach ($educacionais as $educacional): ?>
                     <li class="texto-slide">
                        <h2><?php echo $educacional->titulo ?></h2>
                        <?php echo $educacional->texto ?>
                     </li>
                  <?php endforeach ?>
               </ul>
   		</div>
   </div>
   <div class="clearfix"></div>
</div>