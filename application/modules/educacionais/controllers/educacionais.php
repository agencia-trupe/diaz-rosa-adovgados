<?php

Class Educacionais extends MX_Controller
{

        public function index()
        {
             $data = array(
                        'titulo' => 'Setor Educacional',
                        'page_title' => 'Setor Educacional',
                        'page_description' => 'Atuação no Setor Educacional',
                        'pagina' => 'setor-educacional',
                        'conteudo_principal' => 'educacionais/index'
                );
             $data['result_count'] = $this->db->get('educacionais')->num_rows();
             $this->load->model('educacionais/educacional');
             $data['educacionais'] = $this->educacional->get_all();
             $this->load->view('layout/template', $data);
        }

}