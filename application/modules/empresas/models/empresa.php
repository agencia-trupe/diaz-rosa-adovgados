<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Pagna
*
*
* @author Nilton de Freitas
* @link http://trupe.net
*
*/
Class Empresa extends Datamapper{
    var $table = 'empresas';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }

    function get_resumo($slug)
    {
        $pagina = new Pagina();
        $pagina->where('slug', $slug);
        $pagina->limit(1);
        $pagina->get();

        $resumo = substr(strip_tags($pagina->texto), 0, 160);

        return $resumo;
    }

    function get_conteudo($id)
    {
        $pagina = new Empresa();
        $pagina->where('id', 1);
        $pagina->limit(1);
        $pagina->get();

        return $pagina;
    }


    function get_home()
    {
        $programa = new Programa();
        $programa->order_by('programa_data_expiracao', 'asc');
        $programa->get(2);

        $arr = array();
        foreach($programa->all as $programas)
        {
            $arr[] = $programas;
        }

        return $arr;

    }


    function atualiza($dados){

        $pagina = new Empresa();
        $pagina->where('id', '1');
        $update = $pagina->update(array(
            'texto' => $dados['texto'],
        ));
        return $update;
    }
    function get_programa_by_id($id)
    {
        $programa = new Programa();
        $programa->where('id', $id);
        $programa->limit(1);
        $programa->get();

        return $programa;
    }

     function delete_programa($id){
        $programa = new Programa();
        $programa->where('id', $id)->get();
        

        if($programa->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    
}