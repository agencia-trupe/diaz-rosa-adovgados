<?php

Class Empresas extends MX_Controller
{

        public function index()
        {
             $data = array(
                        'titulo' => 'Quem Somos',
                        'page_title' => 'Quem Somos',
                        'page_description' => 'Quem somos e o que fazemos',
                        'pagina' => 'empresa',
                        'conteudo_principal' => 'empresas/index'
                );
             $this->load->model('empresas/empresa');
             $data['conteudo'] = $this->empresa->get_conteudo('1');
             $this->load->view('layout/template', $data);
        }

}