<div class="conteudo empresa">
   <h1 class="titulo-pagina">Quem Somos</h1>
   <div class="conteudo-pagina">
   		<div class="empresa-imagem-wrapper">
   			<img src="<?php echo base_url('assets/img/quem-somos.jpg') ?>" alt="Diaz Rosa - Quem Somos">
   		</div>
   		<div class="pagina-texto">
   			<?php echo $conteudo->texto ?>
   		</div>
   </div>
   <div class="clearfix"></div>
</div>