<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <?php echo $this->seo->get_title($titulo); ?>
        <meta name="description" content="Prosperativa é um núcleo de expertises
         que tem o propósito de contribuir com a Inteligência das empresas para 
         que conquistem um novo patamar de prosperidade.">
        <meta name="keywords" content="Consultoria, negócio, estratégias, 
        objetivos, metas, educação, liderança, gestão, processos, resultados.">
        <meta name="author" content="Trupe Agência Criativa - http://trupe.net">
        <meta name="viewport" content="width=device-width">
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <header>
            <div class="linha-topo"></div>
                <div class="clearfix"></div>
                <div class="marca-contato">
                    <div class="inner">
                        <a href="<?php echo base_url(); ?>" class="marca">
                        </a>
                        <div class="contato-header">
                            <div class="clearfix"></div>
                            <?php echo Modules::run('contato/endereco', 'header'); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <nav class="<?php echo $pagina ?>">
                    <div class="clearfix"></div>
                    <div class="inner">
                        <ul>
                            <li class="first"><a <?php echo ($pagina == 'home') ? 'class="active"' : ''; ?> id="home" href="<?php echo site_url(); ?>">Home</a></li>
                            <li><a <?php echo ($pagina == 'empresa') ? 'class="active"' : ''; ?> id="quem-somos" href="<?php echo site_url('quem-somos'); ?>">Quem Somos</a></li>
                            <li><a <?php echo ($pagina == 'setor-educacional') ? 'class="active"' : ''; ?> id="setor-educacional" href="<?php echo site_url('setor-educacional'); ?>">Setor Educacional</a></li>
                            <li><a <?php echo ($pagina == 'consultoria-juridica') ? 'class="active"' : ''; ?> id="consultoria-juridica" href="<?php echo site_url('consultoria-juridica-empresarial'); ?>">Consultoria Jurídica Empresarial</a></li>
                            <li><a <?php echo ($pagina == 'atuacao') ? 'class="active"' : ''; ?> id="atuacao" href="<?php echo site_url('atuacao'); ?>">Áreas de Atuação</a></li>
                            <li><a <?php echo ($pagina == 'contato') ? 'class="active"' : ''; ?>id="contato" href="<?php echo site_url('contato'); ?>">Contato</a></li> 
                        </ul>
                    </div>
                </nav>
                <div class="clearfix"></div>
        </header>
        <div class="linha-header"></div>
        <div id="main" class="<?php echo $pagina; ?>">
            <div class="inner">