        </div>
    </div>
    <div class="clearfix"></div>
    <footer>
        <div class="inner">
            <nav class="footer">
                <ul class="esquerda">
                    <li><a href="<?php echo site_url(); ?>"><span class="seta">&raquo;</span>Home</a></li>
                    <li><a href="<?php echo site_url('quem-somos'); ?>"><span class="seta">&raquo;</span>Quem Somos</a></li>
                    <li><a href="<?php echo site_url('setor-educacional'); ?>"><span class="seta">&raquo;</span>Setor Educacional</a></li>
                </ul>
                <ul class="direita">
                    <li><a href="<?php echo site_url('consultoria-juridica-empresarial'); ?>"><span class="seta">&raquo;</span>Consultoria Jurídica Empresarial</a></li>
                    <li><a href="<?php echo site_url('atuacao'); ?>"><span class="seta">&raquo;</span>Areas de Atuação</a></li>
                    <li><a href="<?php echo site_url('contato'); ?>"><span class="seta">&raquo;</span>Contato</a></li>
                </ul>
            </nav>
            <div class="endereco-rodape">
                <?php echo Modules::run('contato/endereco', 'rodape') ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="copy">
            <div class="inner">
                <span class="copy-text">&copy2013 Diaz Rosa &middot; Todos os direitos reservados</span>
                <a class="trupe" href="http://trupe.net" target="_blank">
                    Criação de sites: Trupe agência criativa
                    <img src="<?php echo base_url('assets/img/kombi.png') ?>" alt="Criação de Sites: Trupe Agência Criativa">
                </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script src="<?php echo base_url(); ?>assets/fancybox/source/jquery.fancybox.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript">
          $(".parceiro-bloco:nth-child(3)").css({'margin-right' : '0'});
        </script>
        </body>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-35751088-1'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>
