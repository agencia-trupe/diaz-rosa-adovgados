<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "static_pages";
$route['quem-somos'] = 'empresas';
$route['perfil'] = 'static_pages/perfil';
$route['servicos'] = 'static_pages/servicos';

$route['setor-educacional'] = 'educacionais';
$route['consultoria-juridica-empresarial'] = 'empresariais';

$route['equipe/(:any)'] = 'equipes/detalhe/$1';
$route['equipe'] = 'equipes';
$route['atuacao/(:any)'] = 'atuacoes/detalhe/$1';
$route['atuacao'] = 'atuacoes';

$route['parceiros'] = 'parceiros/parceiros';
$route['parceiros/parceiros/(:num)'] = 'parceiros/lista/parceiros/$1';
$route['parceiros/obras-artigos'] = 'parceiros/lista/obras-artigos';
$route['parceiros/reportagens-concedidas'] = 'parceiros/lista/reportagens-concedidas';
$route['parceiros/obras-artigos/(:num)'] = 'parceiros/lista/obras-artigos/$1';
$route['parceiros/reportagens-concedidas/(:num)'] = 'parceiros/lista/reportagens-concedidas/$1';

$route['agenda'] = 'eventos';
$route['agenda/proximos'] = 'eventos/lista/proximos';
$route['agenda/proximos/(:any)'] = 'eventos/lista/proximos/$1';
$route['agenda/realizados'] = 'eventos/lista/realizados';
$route['agenda/realizados/(:any)'] = 'eventos/lista/realizados/$1';

$route['painel/apresentacao'] = 'admin_apresentacao';
$route['painel/apresentacao/(:any)'] = 'admin_apresentacao/$1';

$route['painel/equipe'] = 'admin_equipe';
$route['painel/equipe/(:any)'] = 'admin_equipe/$1';

$route['painel/atuacao'] = 'admin_atuacoes';
$route['painel/atuacao/(:any)'] = 'admin_atuacoes/$1';

$route['painel/parceiros'] = 'admin_parceiros';
$route['painel/parceiros/(:any)'] = 'admin_parceiros/$1';

$route['painel/educacional'] = 'admin_educacionais';
$route['painel/educacional/(:any)'] = 'admin_educacionais/$1';

$route['painel/empresarial'] = 'admin_empresariais';
$route['painel/empresarial/(:any)'] = 'admin_empresariais/$1';

$route['painel/contato'] = 'admin_contato';
$route['painel/contato/(:any)'] = 'admin_contato/$1';

$route['painel/boxes'] = 'admin_boxes';
$route['painel/boxes/(:any)'] = 'admin_boxes/$1';

$route['painel/empresa'] = 'admin_apresentacao';
$route['painel/empresa/(:any)'] = 'admin_apresentacao/$1';

$route['painel/slideshow'] = 'admin_slideshow';
$route['painel/slideshow/(:any)'] = 'admin_slideshow/$1';

$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';

$route['painel/links'] = 'admin_links';
$route['painel/links/(:any)'] = 'admin_links/$1';

$route['404_override'] = '';

$route['painel'] = 'admin_parceiros';


/* End of file routes.php */
/* Location: ./application/config/routes.php */