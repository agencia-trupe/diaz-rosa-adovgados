<div class="conteudo atuacao">
    <h1 class="titulo-pagina">Áreas de Atuação</h1>
    <div class="conteudo-pagina">
        <div class="pagina-sidebar">
            <div class="pagina-nav-wrapper">
                        <?php for ($i=0; $i < $result_count; $i++): ?>
                            <a  data-slide-index="<?php echo $i ?>" href="#"><?php echo $atuacoes[$i]->titulo ?></a></li>
                        <?php endfor ?>
                </nav>
            </div>
            <div class="clearfix"></div>
        </div>
   		<div class="pagina-texto">
               <ul class="pagina-slider">
                  <?php foreach ($atuacoes as $atuacao): ?>
                     <li class="texto-slide">
                        <h2><?php echo $atuacao->titulo ?></h2>
                        <div class="atuacao-imagem-wrapper">
                          <img src="<?php echo base_url('assets/img/' . $atuacao->imagem ) ?>" alt="<?php echo $atuacao->imagem ?>">
                        </div>
                        <?php echo $atuacao->texto ?>
                     </li>
                  <?php endforeach ?>
               </ul>
   		</div>
   </div>
   <div class="clearfix"></div>
</div>